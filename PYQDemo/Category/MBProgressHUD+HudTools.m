//
//  MBProgressHUD+ShowSuccessHud.m
//  CoolMove
//
//  Created by CA on 15/4/17.
//  Copyright (c) 2015年 CA. All rights reserved.
//

#import "MBProgressHUD+HudTools.h"

@implementation MBProgressHUD (HudTools)

+ (void)showSuccessHudInView:(UIView *)view message:(NSString *)message afterDelay:(NSTimeInterval)delay
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    hud.mode = MBProgressHUDModeCustomView;
    hud.labelText = message;
    [hud hide:YES afterDelay:delay];
}

+ (void)showFailHudInView:(UIView *)view message:(NSString *)message afterDelay:(NSTimeInterval)delay
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Fail.png"]];
    hud.mode = MBProgressHUDModeCustomView;
    hud.labelText = message;
    [hud hide:YES afterDelay:delay];
}

+ (void)showTextHUDWithMessage:(NSString *)message inView:(UIView *)view
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = message;
    hud.margin = 10.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:1.0f];
}

- (void)showSuccessHudInView:(UIView *)view message:(NSString *)message afterDelay:(NSTimeInterval)delay
{
    self.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    self.mode = MBProgressHUDModeCustomView;
    self.labelText = message;
    [self hide:YES afterDelay:delay];
}

- (void)showFailHudInView:(UIView *)view message:(NSString *)message afterDelay:(NSTimeInterval)delay
{
    self.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Fail.png"]];
    self.mode = MBProgressHUDModeCustomView;
    self.labelText = message;
    [self hide:YES afterDelay:delay];
}

- (void)showTextHUDWithMessage:(NSString *)message inView:(UIView *)view
{
    self.mode = MBProgressHUDModeText;
    self.labelText = message;
    self.margin = 10.f;
    self.removeFromSuperViewOnHide = YES;
    [self hide:YES afterDelay:1.0f];
}

- (void)showImageName:(NSString *)imageName
          message:(NSString *)message
           inView:(UIView *)view
       afterDelay:(NSTimeInterval)delay
{
    self.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    self.mode = MBProgressHUDModeCustomView;
    self.labelText = message;
    [self hide:YES afterDelay:delay];
}

@end
