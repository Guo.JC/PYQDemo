//
//  MBProgressHUD+ShowSuccessHud.h
//  CoolMove
//
//  Created by CA on 15/4/17.
//  Copyright (c) 2015年 CA. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (ShowSuccessHud)
+ (void)showSuccessHudInView:(UIView *)view message:(NSString *)message afterDelay:(NSTimeInterval)delay;
+ (void)showFailHudInView:(UIView *)view message:(NSString *)message afterDelay:(NSTimeInterval)delay;
+ (void)showTextHUDWithMessage:(NSString *)message inView:(UIView *)view;
- (void)showTextHUDWithMessage:(NSString *)message inView:(UIView *)view;
- (void)showSuccessHudInView:(UIView *)view message:(NSString *)message afterDelay:(NSTimeInterval)delay;
- (void)showFailHudInView:(UIView *)view message:(NSString *)message afterDelay:(NSTimeInterval)delay;
- (void)showImageName:(NSString *)imageName message:(NSString *)message inView:(UIView *)view afterDelay:(NSTimeInterval)delay;
@end
