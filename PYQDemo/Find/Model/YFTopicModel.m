//
//   同步分享到 YFTopicModel.m
//  PhotoManager
//
//  Created by Coollang on 2017/9/5.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import "YFTopicModel.h"

@implementation YFTopicModel
- (instancetype)initWithType:(YFTopicModelType)type {
    if (self = [super init]) {
        self.type = type;
    }
    return self;
}

@end
