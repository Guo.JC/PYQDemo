//
//   同步分享到 YFTopicModel.h
//  PhotoManager
//
//  Created by Coollang on 2017/9/5.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HXPhotoModel.h"


typedef enum : NSUInteger {
    kYFTopicModelMediaTypePhoto,
    kYFTopicModelMediaTypeVideo,    // 视频
    kYFTopicModelMediaTypeText,     // 文字
    kYFTopicModelMediaTypeAddPhoto, // 加号
    kYFTopicModelMediaTypeSynShare, // 同步分享到
} YFTopicModelType;

@interface YFTopicModel : NSObject

/** 模型对象类型 */
@property (nonatomic, assign)YFTopicModelType type;

- (instancetype)initWithType:(YFTopicModelType)type;

// 类型为：kYFTopicModelMediaTypePhoto 才有用
@property (nonatomic, strong) HXPhotoModel *photoModel;



@end
