//
//  JCMomentSetting.h
//  PYQDemo
//
//  Created by Guo.JC on 2017/9/7.
//  Copyright © 2017年 coollang. All rights reserved.
//

#ifndef JCMomentSetting_h
#define JCMomentSetting_h

#define     kTextLabelWidth                 (kWidth - 85)
#define     kTextLabelNormalHeight          84
#define     kWatchMoreBtnHeight             30
#define     kImageHeight                    (kAutoWid(85))
#define     kImageMaxWidth                  (0.8*kTextLabelWidth)
#define     kImageMinWidth                  150
#define     kTextFont                       14

#define     kNickNameHeight                 46
#define     kShowTextBtnHeight              30
#define     kHideTextBtnHeight              10
#define     kValidAdressHeight              45
#define     kAvalidAdressHeight             22
#define     kBottomSpaceHeight              6
#define     kBottomHeight                   56 ///底部点赞+评论+间隙高度（临时用）

#define     kCommentWidth                   160

#define     kNoticeCancelAllEdit        @"cancelAllEdit"
#define     kNoticePopInputView         @"popInputView"
#define     kNoticePopMenu              @"popMenuView"
#define     kNoticePopDeleteComment     @"popDeleteCommentView"

#endif /* JCMomentSetting_h */
