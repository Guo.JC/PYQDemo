//
//  JCLikeListModel.m
//  Victor
//
//  Created by Guo.JC on 2017/9/1.
//  Copyright © 2017年 coollang. All rights reserved.
//

#import "JCLikeListModel.h"
#import "NSString+CheckIsString.h"

@implementation JCLikeListModel

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"userID": @"UserID",
                                                                  @"userName": @"UserName"
                                                                  }];
}

/**
 创建点赞人模型
 @param         sourceArray         源数据，网络请求回来的数组
 @return                            解析好的数组模型
 */
+ (NSArray <JCLikeListModel *> *)creatModelWithArray:(NSArray <NSDictionary *> *)sourceArray{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *sourceDic in sourceArray) {
        JCLikeListModel *model = [JCLikeListModel new];
        model.userID = [NSString checkIfNullWithString:sourceDic[@"UserID"]];
        model.userName = [NSString checkIfNullWithString:sourceDic[@"UserName"]];
        [array addObject:model];
    }
    return [array mutableCopy];
}

@end
