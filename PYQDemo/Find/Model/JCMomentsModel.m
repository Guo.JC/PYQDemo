//
//  JCMomentsModel.m
//  PYQDemo
//
//  Created by Guo.JC on 2017/9/2.
//  Copyright © 2017年 coollang. All rights reserved.
//

#import "JCMomentsModel.h"
#import "NSString+CheckIsString.h"
#import "JCLocation.h"
#import "JCLikeListModel.h"
#import <YYKit/YYKit.h>
#import <TYAttributedLabel/TYAttributedLabel.h>
#import "JCUserManager.h"

@implementation JCMomentsModel

/**
 创建帖子数据模型
 @param         sourceArray         源数据，网络请求回来的数组
 @return                            解析好的数组模型
 */
+ (NSArray <JCMomentsModel *> *)creatModelWithArray:(NSArray <NSDictionary *> *)sourceArray{
    NSMutableArray *modelArray = [NSMutableArray array];
    for (NSDictionary *sourceDic in sourceArray) {
        JCMomentsModel *model = [JCMomentsModel new];
        model.userName = [NSString checkIfNullWithString:sourceDic[@"UserName"]];
        model.icon = [NSString checkIfNullWithString:sourceDic[@"Icon"]];
        model.userID = [NSString checkIfNullWithString:sourceDic[@"UserID"]];
        model.topicID = [NSString checkIfNullWithString:sourceDic[@"ID"]];
        model.text = [NSString checkIfNullWithString:sourceDic[@"Text"]];
        NSString *position = [NSString checkIfNullWithString:sourceDic[@"Position"]];
        model.longitude = [position componentsSeparatedByString:@","].firstObject.doubleValue;
        model.latitude = [position componentsSeparatedByString:@","].lastObject.doubleValue;
        model.createTime = [NSString checkIfNullWithString:sourceDic[@"CreateTime"]];
        model.images = sourceDic[@"ImgList"];
        model.likeList = [NSMutableArray arrayWithArray:[JCLikeListModel creatModelWithArray:sourceDic[@"LikeList"]]];
        model.isLike = [NSString checkIfNullWithString:sourceDic[@"IsLiked"]].boolValue;
        model.responseList = [JCMomentResponseModel creatModelWithArray:sourceDic[@"ResponseList"] momentModel:model];
        ///计算图片高度、文本高度、时间地址高度
        model.imagesHeight = [model caculImageViewHeight];
        model.textLabelHeight = [model caculLabelWithString:model.text width:kTextLabelWidth font:kTextFont];
        model.timeAdressHeight = model.latitude?kValidAdressHeight:kAvalidAdressHeight;
        ///计算点赞列表的高度及生成点赞字符串
        [model caculLikeHeight];
        ///计算评论所需高度
        [model calculCommetHeight];
        ///计算cell高度
        [model caucalCellHeight];
        model.showMoreSate = ShowMoreBtnSatePackUp;
        model.isMyMoment = [model.userID isEqualToString:[JCUser currentUerID]]?YES:NO;
        [modelArray addObject:model];
    }
    return [modelArray mutableCopy];
}

/**
 计算label显示需要的高度
 */
- (CGFloat)caculLabelWithString:(NSString *)string width:(CGFloat)width font:(CGFloat)font{
//    UILabel *titleLabel = [UILabel new];
//    titleLabel.font = [UIFont systemFontOfSize:font];
//    titleLabel.text = string;
//    titleLabel.numberOfLines = 0;//多行显示，计算高度
//    titleLabel.textColor = [UIColor lightGrayColor];
//    CGSize size = [string boundingRectWithSize:CGSizeMake(width, MAXFLOAT)
//                                       options:NSStringDrawingUsesLineFragmentOrigin
//                                    attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:font]} context:nil].size;
//    return size.height;
    //计算文本尺寸
    YYTextContainer  *titleContarer = [YYTextContainer new];
    titleContarer.size = CGSizeMake(width,CGFLOAT_MAX);
    NSMutableAttributedString  *titleAttr = [self getAttr:string];
    YYTextLayout *titleLayout = [YYTextLayout layoutWithContainer:titleContarer text:titleAttr];
    CGFloat titleLabelHeight = titleLayout.textBoundingSize.height;
    return titleLabelHeight;
}

- (NSMutableAttributedString*)getAttr:(NSString*)attributedString {
    NSMutableAttributedString * resultAttr = [[NSMutableAttributedString alloc] initWithString:attributedString];
    //对齐方式 这里是 两边对齐
    resultAttr.alignment = NSTextAlignmentJustified;
    //设置行间距
    if (iPhone5) {
        resultAttr.lineSpacing = 1.4;
    }else if (iPhone6){
        resultAttr.lineSpacing = 2.48;
    }else if (iPhonePlus){
        resultAttr.lineSpacing = 2;
    }
    //设置字体大小
    resultAttr.font = [UIFont systemFontOfSize:kTextFont];
    //可以设置某段字体的大小
    //[resultAttr yy_setFont:[UIFont boldSystemFontOfSize:CONTENT_FONT_SIZE] range:NSMakeRange(0, 3)];
    //设置字间距
    //resultAttr.yy_kern = [NSNumber numberWithFloat:1.0];
    return resultAttr;
}

/**
 计算图片显示需要的高度
 */
- (CGFloat)caculImageViewHeight{
    NSInteger count = self.images.count;
    NSInteger height = 0;
    self.isLongImage = NO;
    if (count == 1) {///一张图
        NSString *urlString = self.images.firstObject;
        NSString *sizeString = [urlString componentsSeparatedByString:@"-"].lastObject;
        CGSize imageSize = CGSizeMake([sizeString componentsSeparatedByString:@"*"].firstObject.floatValue, [sizeString componentsSeparatedByString:@"*"].lastObject.floatValue);
        CGFloat ratio;
        
        if (imageSize.width > imageSize.height){
            ///横图处理
            ratio = (CGFloat)imageSize.width / imageSize.height;
            if (imageSize.width > 2.0 * imageSize.height) {
                ///长横图
                CGFloat W2 = kImageMaxWidth;
                imageSize.height = (W2 * imageSize.height)/imageSize.width;
                imageSize.width = W2;
                if (imageSize.height < 30) {
                    imageSize.height = 30;
                }
                self.isLongImage = YES;
            }else{
                ///一般
                if (imageSize.width / 2.0 < kTextLabelWidth) {
                    ///小于屏宽
                    if (imageSize.width / 2.0 < kImageMinWidth) {
                        CGFloat W2 = kImageMinWidth;
                        imageSize.height = (W2 * imageSize.height)/imageSize.width;
                        imageSize.width = W2;
                    }else{
                        ratio = 2.0;
                        imageSize.width = imageSize.width / ratio;
                        imageSize.height = imageSize.height / ratio;
                    }
                }else{
                    ///大于屏宽
                    CGFloat W2 = kImageMaxWidth;
                    ///H2=(W2*H1)/W1
                    imageSize.height = (W2 * imageSize.height)/imageSize.width;
                    imageSize.width = W2;//宽为最大
                }
            }
        }
        
        else if(imageSize.width < imageSize.height){
            ///长图处理
            if (imageSize.height / imageSize.width > 2) {
                ///长图片
                CGFloat W2 = 0.4*kImageMaxWidth;
                ///H2=(W2*H1)/W1
                imageSize.height = (W2 * imageSize.height)/imageSize.width;
                imageSize.width = W2;
                if (imageSize.height > imageSize.width * 3) {
                    imageSize.height = imageSize.width * 3;
                }
                self.isLongImage = YES;
            }else{
                ///普通竖直方向图片
                ratio = (CGFloat)imageSize.width / imageSize.height;//宽高比
                if (imageSize.width / 2.0 < kTextLabelWidth) {
                    ///小于屏宽
                    if (imageSize.width / 2.0 < kImageMinWidth) {
                        CGFloat W2 = kImageMinWidth;
                        imageSize.height = (W2 * imageSize.height)/imageSize.width;
                        imageSize.width = W2;
                    }else{
                        ratio = 2.0;
                        imageSize.width = imageSize.width / ratio;
                        imageSize.height = imageSize.height / ratio;
                    }
                }else{
                    ///大于屏宽
                    CGFloat W2 = kImageMaxWidth;
                    ///H2=(W2*H1)/W1
                    imageSize.height = (W2 * imageSize.height)/imageSize.width;
                    imageSize.width = W2;//宽为最大
                }
            }
        }
        else{
            ///相等处理
            imageSize = CGSizeMake(kImageMinWidth, kImageMinWidth);
        }
        
        self.singleImageSize = imageSize;
        height = imageSize.height;
    }else if(count == 2 || count == 3){
        height = kImageHeight;
    }else{
        height = 2*kImageHeight + 4;
    }
    return height;
}

/**
 计算点赞需要的高度
 生成相应的字符串
 */
- (void)caculLikeHeight{
    NSMutableArray *sourceText = [NSMutableArray array];
    NSMutableArray *sourceID = [NSMutableArray array];
    [sourceText addObject:@"    "];
    [sourceID addObject:@""];
    for (JCLikeListModel *model in self.likeList) {
        [sourceText addObject:model.userName?model.userName:@"未知名字"];
        [sourceID addObject:model.userID];
    }
    NSArray *sourceArray = [sourceText mutableCopy];
    NSString *textData;
    BOOL isFirst = YES;
    for (NSString *text in sourceArray) {
        if (textData == nil) {
            textData = text;
        }else{
            if (isFirst) {
                textData = [textData stringByAppendingString:[NSString stringWithFormat:@" %@",text]];
                isFirst = NO;
            }else{
                textData = [textData stringByAppendingString:[NSString stringWithFormat:@"、%@",text]];
            }
        }
    }
    // 属性文本生成器
    TYTextContainer *textContainer = [[TYTextContainer alloc]init];
    textContainer.text = textData;
    
    for (NSInteger i = 0 ; i < sourceArray.count; i++) {
        NSRange range;
        NSString *preString;
        NSString *needString = sourceArray[i];
        NSArray *subArray = [sourceArray subarrayWithRange:NSMakeRange(0, i)];
        for (NSString *sub in subArray) {
            if (preString == nil) {
                preString = sub;
            }else{
                preString = [preString stringByAppendingString:[NSString stringWithFormat:@"、%@",sub]];
            }
        }
        NSInteger preStringLength = preString.length;
        if (preStringLength == 0) {
            range = NSMakeRange(preStringLength, needString.length);
        }else{
            range = NSMakeRange(preStringLength+1, needString.length);
        }
        [textContainer addLinkWithLinkData:sourceID[i]
                                 linkColor:UIColorFromHex(0x576b95)
                            underLineStyle:kCTUnderlineStyleNone
                                     range:range];
    }
    
    if (self.likeList && self.likeList.count > 0) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
        imageView.image = [UIImage imageNamed:@"find_like_list"];
        [textContainer addView:imageView range:NSMakeRange(0,4)];
    }
    textContainer.linesSpacing = 4;
    textContainer.font = [UIFont boldSystemFontOfSize:13];
    textContainer = [textContainer createTextContainerWithTextWidth:kTextLabelWidth - 16];
    self.likeString = textContainer;
    self.likeHeight = textContainer.textHeight;
    if (self.likeList.count == 0 || self.likeList == nil) {
        self.likeString = nil;
        self.likeHeight = 0;
    }
}

/**
 计算评论需要的高度
 */
- (void)calculCommetHeight{
    self.commentHeigh = 0;
    for (JCMomentResponseModel *model in self.responseList) {
        self.commentHeigh += model.commentHeight;
    }
}

/**
 点赞、取消赞
 @param         state        YES:点赞 NO:取消赞
 */
- (void)editLikeState:(BOOL)state{
    JCUser *user = [JCUser currentUer];
    if (state) {
        ///点赞
        JCLikeListModel *model = [JCLikeListModel new];
        model.userID = user.userID;
        model.userName = user.userInfo.nickName;
        [self.likeList addObject:model];
    }else{
        ///取消赞
        JCLikeListModel *remove;
        for (JCLikeListModel *model in self.likeList) {
            if ([model.userID isEqualToString:user.userID]) {
                remove = model;
            }
        }
        [self.likeList removeObject:remove];
    }
    ///重新计算高度、生成点赞列表字符串
    [self caculLikeHeight];
    ///计算cell高度
    [self caucalCellHeight];
}

/**
 添加一条评论
 @param         model        评论数据模型
 */
- (void)addCommentModel:(JCMomentResponseModel *)model{
    model.momentModel = self;
    ///添加新的数据
    [self.responseList addObject:model];
    ///计算评论所需高度
    [self calculCommetHeight];
    ///计算cell高度
    [self caucalCellHeight];
}

/**
 添加一条回复
 @param         model        回复数据模型
 */
- (void)addResponseModel:(JCMomentResponseModel *)model{
    model.momentModel = self;
    ///添加新的数据
    [self.responseList addObject:model];
    ///计算评论所需高度
    [self calculCommetHeight];
    ///计算cell高度
    [self caucalCellHeight];
}

- (void)caucalCellHeight{
    NSInteger space;
    if (self.likeHeight == 0 && self.commentHeigh == 0) {
        space = 8;
    }else{
        space = 16;
    }
    if (self.textLabelHeight > kTextLabelNormalHeight) {
        ///需要显示更多
        self.isNeedShowMoreBtn = YES;
        self.normalCellHeight = kNickNameHeight + kTextLabelNormalHeight + kShowTextBtnHeight + self.imagesHeight + self.timeAdressHeight + (kBottomSpaceHeight + self.likeHeight + space + self.commentHeigh);
        self.showMoreCellHeight = kNickNameHeight + self.textLabelHeight + kShowTextBtnHeight + self.imagesHeight + self.timeAdressHeight + (kBottomSpaceHeight + self.likeHeight + space + self.commentHeigh);
    }else{
        ///无需显示更多
        self.isNeedShowMoreBtn = NO;
        self.normalCellHeight = self.showMoreCellHeight = kNickNameHeight + self.textLabelHeight + kHideTextBtnHeight + self.imagesHeight + self.timeAdressHeight + (kBottomSpaceHeight + self.likeHeight + space + self.commentHeigh);
    }
}

@end











@implementation JCMomentResponseModel

/**
 创建回复列表模型
 @param         sourceArray         源数据，网络请求回来的数组
 @param         momentModel         帖子数据模型
 @return                            解析好的数组模型
 */
+ (NSMutableArray <JCMomentResponseModel *> *)creatModelWithArray:(NSArray <NSDictionary *> *)sourceArray momentModel:(JCMomentsModel *)momentModel{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *sourceDic in sourceArray) {
        JCMomentResponseModel *model = [JCMomentResponseModel new];
        model.momentModel = momentModel;
        model.postID = [NSString checkIfNullWithString:sourceDic[@"PostID"]];
        model.responseID = [NSString checkIfNullWithString:sourceDic[@"RID"]];
        model.creatTime = [NSString checkIfNullWithString:sourceDic[@"CreateTime"]];
        model.text = [NSString checkIfNullWithString:sourceDic[@"Text"]];
        model.parentID = [NSString checkIfNullWithString:sourceDic[@"ParentID"]];
        model.rUserID = [NSString checkIfNullWithString:sourceDic[@"RUserID"]];
        model.rUserName = [NSString checkIfNullWithString:sourceDic[@"RUserName"]];
        model.quote = [JCMomentQuotoModel creatModelWithDictionary:sourceDic[@"Quote"]];
        [model caucalCommentHeight];
        [array addObject:model];
    }
    return array;
}

/**
 计算评论需要的高度,生成评论文字
 */
- (void)caucalCommentHeight{
    // 属性文本生成器
//    TYTextContainer *textContainer = [[TYTextContainer alloc]init];
//    NSString *text = [NSString stringWithFormat:@"%@:%@",self.rUserName,self.text];
//    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:text];
//    [str addAttribute:NSForegroundColorAttributeName value:UIColorFromHex(0x576b95) range:NSMakeRange(0, self.rUserName.length)];
//    [str addAttribute:NSForegroundColorAttributeName value:UIColorFromHex(0x333333) range:NSMakeRange(self.rUserName.length, self.text.length)];
//    [str addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(0, self.rUserName.length)];
//    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(self.rUserName.length, self.text.length)];
//    textContainer.attributedText = str;
//    [textContainer addLinkWithLinkData:self.rUserName
//                             linkColor:UIColorFromHex(0x576b95)
//                        underLineStyle:kCTUnderlineStyleNone
//                                 range:NSMakeRange(0, self.rUserName.length)];
//    textContainer.linesSpacing = 5;
//    textContainer = [textContainer createTextContainerWithTextWidth:kTextLabelWidth];
//    self.commentString = textContainer;
//    self.commentHeight = textContainer.textHeight + 10;
    
    NSString *userNameLink;
    NSString *commentText;
    NSMutableAttributedString *text;
    CGRect rect;
    if (self.quote == nil) {
        ///是一条评论
        userNameLink = [NSString stringWithFormat:@"%@:",self.rUserName];
        commentText = self.text;
        text  = [[NSMutableAttributedString alloc] initWithString:[userNameLink stringByAppendingString:commentText]];
    }else{
        ///是一条回复
        userNameLink = [NSString stringWithFormat:@"%@回复%@:",self.rUserName,self.quote.userName];
        commentText = self.text;
        text  = [[NSMutableAttributedString alloc] initWithString:[userNameLink stringByAppendingString:commentText]];
    }
    text.lineSpacing = 2;
    text.font = [UIFont systemFontOfSize:kTextFont];
    text.color = [UIColor darkTextColor];
    rect = [text boundingRectWithSize:CGSizeMake(kTextLabelWidth, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    self.commentString = text;
    self.commentHeight = rect.size.height + 10;
}

/**
 创建评论模型
 @param         text                评论的内容
 @param         responseID          服务器返回的评论ID
 @return                            数据模型
 */
+ (JCMomentResponseModel *)creatNewCommentWithText:(NSString *)text
                                            postID:(NSString *)postID
                                        responseID:(NSString *)responseID{
    JCMomentResponseModel *model = [JCMomentResponseModel new];
    model.text = text;
    model.postID = postID;
    model.responseID = responseID;
    model.rUserID = [JCUser currentUerID];
    model.rUserName = [JCUser currentUer].userInfo.nickName;
    [model caucalCommentHeight];
    return model;
}

/**
 创建回复模型
 @param         text                回复的内容
 @param         responseID          服务器返回的评论ID
 @param         commentModel        回复的那条评论的数据模型
 @return                            数据模型
 */
+ (JCMomentResponseModel *)creatNewResponseWithText:(NSString *)text
                                         responseID:(NSString *)responseID
                                       commentModel:(JCMomentResponseModel *)commentModel{
    JCMomentResponseModel *model = [JCMomentResponseModel new];
    model.momentModel = commentModel.momentModel;
    model.text = text;
    model.postID = commentModel.postID;
    model.responseID = responseID;
    model.parentID = commentModel.responseID;
    model.rUserID = [JCUser currentUerID];
    model.rUserName = [JCUser currentUer].userInfo.nickName;
    JCMomentQuotoModel *quoto = [JCMomentQuotoModel new];
    quoto.userID = commentModel.rUserID;
    quoto.userName = commentModel.rUserName;
    model.quote = quoto;
    [model caucalCommentHeight];
    return model;
}

@end











@implementation JCMomentQuotoModel

/**
 创建回复谁的评论
 @param         sourceDic           源数据
 @return                            解析好的模型
 */
+ (instancetype)creatModelWithDictionary:(NSDictionary *)sourceDic{
    if (sourceDic.count == 0 || sourceDic == nil) {
        return nil;
    }
    JCMomentQuotoModel *quote = [JCMomentQuotoModel new];
    quote.userID = [NSString checkIfNullWithString:sourceDic[@"UserID"]];
    quote.userName = [NSString checkIfNullWithString:sourceDic[@"UserName"]];
    return quote;
}

@end
