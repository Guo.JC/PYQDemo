//
//  JCMomentImageView.m
//  PYQDemo
//
//  Created by Guo.JC on 2017/9/4.
//  Copyright © 2017年 coollang. All rights reserved.
//

#import "JCMomentImageView.h"
#import "JCMomentsModel.h"
#import "Masonry.h"
#import "YFPhotoBrowser.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "YYPhotoGroupView.h"

@interface JCMomentImageView ()

@property (nonatomic , strong) JCMomentsModel *model;
@property (nonatomic , strong) NSMutableArray <UIImageView *> *imageViewArray;

@end

@implementation JCMomentImageView

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
}

- (void)initCode{
    if (_imageViewArray == nil) {
        _imageViewArray = [NSMutableArray array];
        for (NSInteger i = 0; i < 6; i++) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            imageView.userInteractionEnabled = YES;
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
            imageView.tag = i;
            ///添加手势
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
            [imageView addGestureRecognizer:tap];
            [self addSubview:imageView];
            [self.imageViewArray addObject:imageView];
        }
    }
}

- (void)setModelData:(JCMomentsModel *)model{
    [self initCode];
    _model = model;
    NSString *param_1 = @"?imageMogr2/thumbnail/102400@";
    NSString *param_2 = @"?imageMogr2/thumbnail/25600@";
    NSInteger count = model.images.count;
    ///一张图
    if (count == 1) {
        ///隐藏1-5
        for (NSInteger i = 1; i < 6; i++) {
            UIImageView *imageView = _imageViewArray[i];
            imageView.frame = CGRectMake(0, 0, 0, 0);
        }
        ///调整0的坐标及尺寸
        UIImageView *imageView = _imageViewArray[0];
        imageView.frame = CGRectMake(0, 0, model.singleImageSize.width, _model.singleImageSize.height);
        NSString *urlString = _model.images.firstObject;
        [imageView sd_setImageWithURL:[NSURL URLWithString:[urlString stringByAppendingString:param_1]]
                     placeholderImage:[UIImage imageNamed:@"placehold_image"]
                            completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                            }];
        UILabel *tipLabel = imageView.subviews.firstObject;
        if (model.isLongImage) {
            tipLabel = imageView.subviews.firstObject;
            tipLabel.hidden = NO;
            if (tipLabel == nil) {
                tipLabel = [UILabel new];
                tipLabel.backgroundColor = UIColorFromHex(0x8599C5);
                tipLabel.textColor = [UIColor whiteColor];
                tipLabel.font = [UIFont systemFontOfSize:12];
                tipLabel.textAlignment = NSTextAlignmentRight;
                tipLabel.text = @"长图";
                [imageView addSubview:tipLabel];
                [tipLabel sizeToFit];
            }
            CGRect frame = tipLabel.frame;
            frame.origin = CGPointMake(imageView.bounds.size.width - frame.size.width, imageView.bounds.size.height - frame.size.height);
            tipLabel.frame = frame;
        }else{
            if (tipLabel) {
                tipLabel.hidden = YES;
            }
        }
    }
    ///2、3、5、6张图
    else if (count == 2 || count == 3 ||  count == 5 || count == 6 || count > 6){
        if (count > 6) {
            count = 6;
        }
        for (NSInteger i = 0; i < 6; i++) {
            UIImageView *imageView = _imageViewArray[i];
            if (i < count) {
                imageView.frame = CGRectMake((i>=3?i-3:i%3)*(kImageHeight+4), 0+(i/3 * (kImageHeight+4)), kImageHeight, kImageHeight);
                [imageView sd_setImageWithURL:[NSURL URLWithString:[self.model.images[i] stringByAppendingString:param_2]]
                             placeholderImage:[UIImage imageNamed:@"placehold_image"]
                                    completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                        
                                    }];
            }else{
                imageView.frame = CGRectMake(0, 0, 0, 0);
            }
        }
    }
    ///4张图
    else if (count == 4){///四张图
        ///隐藏 4 - 5
        for (NSInteger i = 4; i < 6; i++) {
            UIImageView *imageView = _imageViewArray[i];
            imageView.frame = CGRectMake(0, 0, 0, 0);
        }
        ///排布0 - 3
        for (NSInteger i = 0; i < 4; i++) {
            UIImageView *imageView = _imageViewArray[i];
            imageView.frame = CGRectMake((i%2)*(kImageHeight+4), 0+((i/2) * (kImageHeight+4)), kImageHeight, kImageHeight);
            [imageView sd_setImageWithURL:[NSURL URLWithString:[self.model.images[i] stringByAppendingString:param_2]]
                         placeholderImage:[UIImage imageNamed:@"placehold_image"]
                                completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                
            }];
        }
    }
}

- (void)tapAction:(UITapGestureRecognizer *)sender{
//    sender.view.contentMode = UIViewContentModeScaleAspectFit;
//    sender.view.clipsToBounds = NO;
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        sender.view.contentMode = UIViewContentModeScaleAspectFill;
//        sender.view.clipsToBounds = YES;
//    });
    
    NSMutableArray *items = [NSMutableArray array];
    UIImageView *fromView = nil;
    for (NSInteger i = 0; i < _model.images.count; i++) {
        NSString *urlString = _model.images[i];
        YYPhotoGroupItem *item = [[YYPhotoGroupItem alloc] init];
        UIImageView *imageView = _imageViewArray[i];
        item.thumbView = imageView;
        item.largeImageURL = [NSURL URLWithString:urlString];
        NSString *sizeString = [urlString componentsSeparatedByString:@"-"].lastObject;
        CGSize imageSize = CGSizeMake([sizeString componentsSeparatedByString:@"*"].firstObject.floatValue, [sizeString componentsSeparatedByString:@"*"].lastObject.floatValue);
        item.largeImageSize = imageSize;
        [items addObject:item];
        if (sender.view == imageView) {
            fromView = imageView;
        }
    }
    YYPhotoGroupView *v = [[YYPhotoGroupView alloc] initWithGroupItems:items];
    [v presentFromImageView:fromView toContainer:[UIApplication sharedApplication].keyWindow animated:YES completion:nil];
    
//    [YFPhotoBrowser browserUrlImages:_model.images fromView:sender.view selectIndex:sender.view.tag];
}

@end
