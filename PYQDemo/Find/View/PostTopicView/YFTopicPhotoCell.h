//
//  YFMoveViewCell.h
//  PhotoManager
//
//  Created by Coollang on 2017/9/4.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YFMoveBaseCell.h"


@interface YFTopicPhotoCell :YFMoveBaseCell

- (void)configerMoveViewCellWithItem:(YFTopicModel *)item;

@end
