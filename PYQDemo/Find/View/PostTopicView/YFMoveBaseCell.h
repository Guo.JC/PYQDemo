//
//  YFMoveBaseCell.h
//  PhotoManager
//
//  Created by Coollang on 2017/9/5.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YFTopicModel.h"

@interface YFMoveBaseCell : UICollectionViewCell

@property (nonatomic, strong) YFTopicModel *model;

@end
