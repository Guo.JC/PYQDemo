//
//  YFTopicSynShareCell.h
//  PhotoManager
//
//  Created by Coollang on 2017/9/5.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YFMoveBaseCell.h"
#import <ShareSDK/ShareSDK.h>

@interface YFTopicSynShareCell : YFMoveBaseCell

/** 点击同步到第三方平台的按钮回调 */
@property (nonatomic, strong) void (^SynButtonClickBlock)(SSDKPlatformType type);

@end
