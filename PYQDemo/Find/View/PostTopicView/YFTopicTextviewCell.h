//
//  YFTopicTextviewCell.h
//  PhotoManager
//
//  Created by Coollang on 2017/9/5.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YFMoveBaseCell.h"

@class BRPlaceholderTextView,YFTopicModel;

@interface YFTopicTextviewCell : YFMoveBaseCell

@property (nonatomic, strong)BRPlaceholderTextView *textview;

@property (nonatomic, strong) YFTopicModel *textModel;

/** 文字改变的回调 */
@property (nonatomic, strong) void(^textViewValueChangeBlock)(NSString *text);


@end
