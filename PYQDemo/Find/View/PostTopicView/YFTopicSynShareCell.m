//
//  YFTopicSynShareCell.m
//  PhotoManager
//
//  Created by Coollang on 2017/9/5.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import "YFTopicSynShareCell.h"


@interface YFTopicSynShareCell ()
@property (weak, nonatomic) IBOutlet UILabel *synLabel;
@property (weak, nonatomic) IBOutlet UIButton *qzoneButton;
@property (weak, nonatomic) IBOutlet UIButton *weChatButton;
@property (weak, nonatomic) IBOutlet UIButton *momentsButton;
@property (weak, nonatomic) IBOutlet UIButton *qqButton;
@property (weak, nonatomic) IBOutlet UILabel *locationDescLabel;

// 当前选中的Button
@property (nonatomic, strong) UIButton *currnetBtn;

@end

@implementation YFTopicSynShareCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor colorWithHex:0xf5f6fd];
    self.synLabel.text = @"同步到";
}

- (IBAction)locationBtnClick:(UIButton *)sender {
}

- (IBAction)btnClickAction:(UIButton *)sender {
    sender.selected = !sender.isSelected;
    
    SSDKPlatformType type = SSDKPlatformTypeUnknown;
    
    if (self.currnetBtn == sender) {
        self.currnetBtn = nil;
    }else {
        self.currnetBtn.selected = NO;
        self.currnetBtn = sender;
        
        switch (self.currnetBtn.tag) {
            case 31:
                type = SSDKPlatformTypeWechat;
                break;
            case 32:// 朋友圈
                type = SSDKPlatformSubTypeWechatTimeline;
                break;
            case 33:
                 type = SSDKPlatformSubTypeQQFriend;
                break;
            case 34:
                type = SSDKPlatformSubTypeQZone;
                break;
        }
    }
    
    if (self.SynButtonClickBlock) {
        self.SynButtonClickBlock(type);
    }
}

@end
