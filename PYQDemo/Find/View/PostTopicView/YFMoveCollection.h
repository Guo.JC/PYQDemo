//
//  YFMoveCollection.h
//  PhotoManager
//
//  Created by Coollang on 2017/9/2.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YFMoveCollection;
@protocol YFMoveCollectionDelegate <NSObject>

@required;
/**
 *  当数据源更新的到时候调用，必须实现，需将新的数据源设置为当前的数据源(例如 :_data = newDataArray)
 *  @param newDataArray   更新后的数据源
 */
- (void)dragCellCollectionView:(YFMoveCollection *)collectionView newDataArrayAfterMove:(NSArray *)newDataArray;
/**
 *  返回整个CollectionView的数据，必须实现，需根据数据进行移动后的数据重排
 */
- (NSArray *)dataSourceArrayOfCollectionView:(YFMoveCollection *)collectionView;

@optional;
/**
 *  cell移动完毕，并成功移动到新位置的时候调用
 */
- (void)dragCellCollectionViewCellEndMoving:(YFMoveCollection *)collectionView;
/**
 *  成功交换了位置的时候调用
 *  @param fromIndexPath    交换cell的起始位置
 *  @param toIndexPath      交换cell的新位置
 */
- (void)dragCellCollectionView:(YFMoveCollection *)collectionView moveCellFromIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath;

@end


@interface YFMoveCollection : UICollectionView

@property (nonatomic, weak) id<YFMoveCollectionDelegate> moveDelegate;
@property (nonatomic, assign)BOOL editEnabled;

@end
