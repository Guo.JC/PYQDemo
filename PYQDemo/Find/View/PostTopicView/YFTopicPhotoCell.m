//
//  YFMoveViewCell.m
//  PhotoManager
//
//  Created by Coollang on 2017/9/4.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import "YFTopicPhotoCell.h"
#import "YFTopicModel.h"

@interface YFTopicPhotoCell ()

//@property (nonatomic, strong) UIImageView  *backImg;
@property (weak, nonatomic) IBOutlet UIImageView *backImageView;
@property (weak, nonatomic) IBOutlet UIView *maskView;
@property (weak, nonatomic) IBOutlet UILabel *videoTimeLabel;

@end

@implementation YFTopicPhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backImageView.contentMode = UIViewContentModeScaleAspectFill;
}

- (void)layoutSubviews {
    [super layoutSubviews];

}

- (void)configerMoveViewCellWithItem:(YFTopicModel *)item {
    self.model = item;
    if (item.type == kYFTopicModelMediaTypePhoto) {
         self.backImageView.image = item.photoModel.thumbPhoto;
        if (item.photoModel.subType == HXPhotoModelMediaSubTypeVideo) {
            self.maskView.hidden = NO;
            self.videoTimeLabel.text = item.photoModel.videoTime;
        }else {
            self.maskView.hidden = YES;
            
        }
        
    }else if(item.type == kYFTopicModelMediaTypeAddPhoto) {
        self.backImageView.image = [UIImage imageNamed:@"post_topic_addIcon"];
    }
    
}

@end
