//
//  YFTopicTextviewCell.m
//  PhotoManager
//
//  Created by Coollang on 2017/9/5.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import "YFTopicTextviewCell.h"
#import "BRPlaceholderTextView.h"
#import "Masonry.h"



@interface YFTopicTextviewCell ()<UITextViewDelegate>

/** 文字字数提示Label */
@property (nonatomic, strong) UILabel *textCountTipLabel;


@end

@implementation YFTopicTextviewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.textview = [[BRPlaceholderTextView alloc] init];
        [self.contentView addSubview:self.textview];
       
        
        self.textCountTipLabel = [[UILabel alloc]init];
        self.textCountTipLabel.font = [UIFont systemFontOfSize:12];
        self.textCountTipLabel.textColor = [UIColor blackColor];
        [self.contentView addSubview:self.textCountTipLabel];
        self.textCountTipLabel.textAlignment = NSTextAlignmentLeft;
        
        [self.textview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(8);
            make.right.equalTo(self.contentView).offset(-8);
            make.top.equalTo(self.contentView).offset(8);
            make.bottom.equalTo(self.textCountTipLabel.mas_top).offset(-8);
        }];
        [self configuerBRPlaceholderTextview];
        
        [self.textCountTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.textview.mas_left).offset(8);
            make.bottom.equalTo(self.contentView).offset(-8);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(20);
        }];
    }
    return self;
}

- (void)configuerBRPlaceholderTextview {
    self.textview.placeholder = @"说下你现在的感受吧...";
    
    [self.textview setPlaceholderColor:[UIColor colorWithHex:0xaaaaaa]];
    
    [self.textview addMaxTextLengthWithMaxLength:3000 andEvent:^(BRPlaceholderTextView *text) {
        // 超过字数限制的回调
        
    }];
    
    [self.textview setPlaceholderOpacity:0.6];
    self.textview.font = [UIFont systemFontOfSize:14];
    
    self.textview.textColor = [UIColor blackColor];
    self.textview.delegate = self;
    
    self.textview.allowsEditingTextAttributes = YES;
    self.textview.keyboardDismissMode = YES;
    self.textview.alwaysBounceVertical = YES;
    self.textview.enablesReturnKeyAutomatically = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewDidValueChange:) name:UITextViewTextDidChangeNotification object:nil];
    self.textCountTipLabel.text = @"";
}

- (void)textViewDidValueChange:(NSNotification *)note {
    
    self.textCountTipLabel.text = [NSString stringWithFormat:@"(%zd/1000)",self.textview.text.length];
    NSInteger textLength = self.textview.text.length;
    if (textLength >= 980 && textLength <= 1000) {
        self.textCountTipLabel.hidden = NO;
        self.textCountTipLabel.text = [NSString stringWithFormat:@"还可以输入%zd字",1000 - textLength];
        self.textCountTipLabel.textColor = [UIColor colorWithHex:0x666666];
    }else if (textLength > 1000){
        self.textCountTipLabel.hidden = NO;
        self.textCountTipLabel.text = [NSString stringWithFormat:@"超过%zd字",textLength - 1000];
        self.textCountTipLabel.textColor = [UIColor colorWithHex:0xFF6E71];
    }else {
        self.textCountTipLabel.hidden = YES;
    }
    
    // 改变发表按钮的状态
    if (self.textViewValueChangeBlock) {
        self.textViewValueChangeBlock(self.textview.text);
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
