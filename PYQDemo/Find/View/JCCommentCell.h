//
//  JCCommentCell.h
//  PYQDemo
//
//  Created by Guo.JC on 2017/9/6.
//  Copyright © 2017年 coollang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JCMomentResponseModel;
@class JCMomentQuotoModel;

@interface JCCommentCell : UITableViewCell

- (void)setMomentResponseModel:(JCMomentResponseModel *)model;

@end
