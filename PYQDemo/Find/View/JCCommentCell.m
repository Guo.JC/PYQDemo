//
//  JCCommentCell.m
//  PYQDemo
//
//  Created by Guo.JC on 2017/9/6.
//  Copyright © 2017年 coollang. All rights reserved.
//

#import "JCCommentCell.h"
#import <TYAttributedLabel/TYAttributedLabel.h>
#import <YYKit/YYKit.h>
#import "JCMomentsModel.h"
#import "JCUserManager.h"

@interface JCCommentCell ()<TYAttributedLabelDelegate>
@property (weak, nonatomic) IBOutlet YYLabel *commentLabel;
@property (strong, nonatomic) JCMomentResponseModel *model;
@end

@implementation JCCommentCell

- (void)setMomentResponseModel:(JCMomentResponseModel *)model{
    _model = model;
    if (model.quote == nil) {
        ///是一条评论
        [model.commentString setTextHighlightRange:NSMakeRange(0, model.rUserName.length)
                                             color:UIColorFromHex(0x576b95)
                                   backgroundColor:[UIColor grayColor]
                                         tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
                                             NSLog(@"%@ 被点击",[[text string] substringWithRange:range]);
                                         }];
    }else{
        ///是一条回复
        [model.commentString setTextHighlightRange:NSMakeRange(0, model.rUserName.length)
                                             color:UIColorFromHex(0x576b95)
                                   backgroundColor:[UIColor grayColor]
                                         tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
                                             NSLog(@"%@ 回复人被点击",[[text string] substringWithRange:range]);
                                         }];
        [model.commentString setTextHighlightRange:NSMakeRange(model.rUserName.length+2, model.quote.userName.length)
                                             color:UIColorFromHex(0x576b95)
                                   backgroundColor:[UIColor grayColor]
                                         tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
                                             NSLog(@"%@ 评论人被点击",[[text string] substringWithRange:range]);
                                         }];
    }
    _commentLabel.attributedText = model.commentString;  //设置富文本
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _commentLabel.numberOfLines = 0;
    _commentLabel.displaysAsynchronously = YES;
    _commentLabel.textAlignment = NSTextAlignmentCenter;
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAction:)];
    [self addGestureRecognizer:longPress];
}

- (void)longPressAction:(UILongPressGestureRecognizer *)sender{
    BOOL isMyComment = [_model.rUserID isEqualToString:[JCUser currentUerID]];
    BOOL isMyTopic = [_model.momentModel.userID isEqualToString:[JCUser currentUerID]];
    BOOL isResponseMyComment = [_model.quote.userID isEqualToString:[JCUser currentUerID]];
    BOOL isAllowDelete = NO;
    if (isMyTopic) {///是我发布的帖子
        if (isMyComment) {
            ///自己的评论
            NSLog(@"自己的帖子，自己的评论，长按删除");
            isAllowDelete = YES;
        }else{
            ///别人的评论
            NSLog(@"自己的帖子，别人的评论，长按删除");
            isAllowDelete = YES;
        }
    }else{///不是我发布的帖子
        if (isMyComment) {
            ///自己的评论
            NSLog(@"别人的帖子，自己的评论，长按删除");
            isAllowDelete = YES;
        }else{
            ///别人的评论
            
        }
    }
    if (isAllowDelete) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNoticePopDeleteComment object:nil userInfo:@{@"deleteModel":_model}];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
