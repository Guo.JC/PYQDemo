//
//  JCMomentsVC.m
//  PYQDemo
//
//  Created by Guo.JC on 2017/9/2.
//  Copyright © 2017年 coollang. All rights reserved.
//

#import "JCMomentsVC.h"
#import "MJRefresh.h"
#import "JCMomentsModel.h"
#import "JCGetLocation.h"
#import <CoreLocation/CoreLocation.h>
#import "JCLocation.h"
#import "JCWebDataRequst+Find.h"
#import <Qiniu/QiniuSDK.h>
#import "JCMomentsCell.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "BRPlaceholderTextView.h"
#import <YYKit/YYKit.h>
#import "SQActionSheetView.h"

@interface JCMomentsVC ()<YYTextViewDelegate,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,JCMomentsCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic, strong) NSMutableArray <JCMomentsModel *> *allMomentModel;
@property (nonatomic, strong) CLGeocoder *clGeoCoder;
@property (nonatomic, strong) CLGeocoder *coder2;
@property (weak, nonatomic) IBOutlet UIView *inputView;
@property (weak, nonatomic) IBOutlet YYTextView *inputTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputViewBottonConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputViewHeightConstraint;
@property (strong, nonatomic) JCMomentResponseModel *responseModel;
@property (strong, nonatomic) JCMomentsModel *momentModel;
@property (assign, nonatomic) NSInteger pageIndex;
@property (assign, nonatomic) BOOL isEdit;

@end

@implementation JCMomentsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initCode];
    [self addNotice];
    [self loadData];
    [self configuerTextview];
}

/**
 初始化
 */
- (void)initCode{
    __weak typeof(self)weakSelf = self;
    _tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf dropRefreshData];
    }];
    _tableview.mj_header.automaticallyChangeAlpha = YES;
    ((MJRefreshNormalHeader *)(_tableview.mj_header)).lastUpdatedTimeLabel.hidden = YES;
    
    _tableview.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
    
    _inputTextView.layer.cornerRadius = 5;
    _inputTextView.layer.masksToBounds = YES;
    _inputViewBottonConstraint.constant = - _inputView.bounds.size.height;
    _inputTextView.layer.borderColor = UIColorFromHex(0x999999).CGColor;
    _inputTextView.layer.borderWidth = 0.5;
}

/**
 注册通知
 */
- (void)addNotice{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(popInputView:)
                                                 name:kNoticePopInputView
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cancelAllEdit)
                                                 name:kNoticeCancelAllEdit
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(popMenu:)
                                                 name:kNoticePopMenu
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(popDeleteComment:)
                                                 name:kNoticePopDeleteComment
                                               object:nil];
}

/**
 删除评论
 */
- (void)popDeleteComment:(NSNotification *)notice{
    __weak typeof(self)weakSelf = self;
    NSDictionary *userInfo = notice.userInfo;
    JCMomentResponseModel *model = userInfo[@"deleteModel"];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"是否删除该条评论？" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [JCWebDataRequst deleteComment:model complete:^(BOOL state) {
            JCMomentsModel *momentModel = model.momentModel;
            [momentModel.responseList removeObject:model];
            ///计算评论所需高度
            [momentModel calculCommetHeight];
            ///计算cell高度
            [momentModel caucalCellHeight];
            [weakSelf.tableview reloadData];
        }];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:delete];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

/**
 帖子右上角弹出菜单
 */
- (void)popMenu:(NSNotification *)notice{
    __weak typeof(self)weakSelf = self;
    NSDictionary *userInfo = notice.userInfo;
    JCMomentsModel *model = userInfo[@"momentModel"];
    NSIndexPath *indexPath = model.indexPath;
    SQActionSheetView *sheetView = [[SQActionSheetView alloc] initWithTitle:@""
                                                                    buttons:@[@"删除",@"取消"]
                                                                     colors:@[[UIColor redColor],[UIColor blackColor]]
                                                                buttonClick:^(SQActionSheetView *sheetView, NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            [JCWebDataRequst deleteTopic:model complete:^(BOOL state) {
                if (state) {
                    JCMomentsModel *deleteModel;
                    for (JCMomentsModel *dataModel in weakSelf.allMomentModel) {
                        if ([dataModel.topicID isEqualToString:model.topicID]) {
                            deleteModel = dataModel;
                        }
                    }
                    ///删除源数据
                    [weakSelf.allMomentModel removeObject:deleteModel];
                    ///删除cell
                    [weakSelf.tableview deleteRow:indexPath.row inSection:indexPath.section withRowAnimation:UITableViewRowAnimationRight];
                }
            }];
        }else if (buttonIndex == 1){
        }
    }];
    [sheetView showView];
}

/**
 配置输入框
 */
- (void)configuerTextview {
    _inputTextView.placeholderFont = _inputTextView.font = [UIFont systemFontOfSize:kTextFont];
    _inputTextView.placeholderTextColor = UIColorFromHex(0x999999);
    _inputTextView.textColor = [UIColor blackColor];
    _inputTextView.delegate = self;
    _inputTextView.returnKeyType = UIReturnKeySend;
}

/**
 取消输入控件的响应
 */
- (void)cancelAllEdit{
    [_inputTextView resignFirstResponder];
    _isEdit = NO;
}

//当键盘出现
- (void)keyboardWillShow:(NSNotification *)notification{
    //获取键盘的高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [value CGRectValue];
    int height = keyboardRect.size.height;
    _inputViewBottonConstraint.constant = height - 49;
    [UIView animateWithDuration:.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.view layoutIfNeeded];
        //        _inputTextView.text = [NSString stringWithFormat:@"@%@",model.rUserName];
    } completion:^(BOOL finished) {
        
    }];
}

//当键退出
- (void)keyboardWillHide:(NSNotification *)notification{
    //获取键盘的高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [value CGRectValue];
    int height = keyboardRect.size.height;
    _inputViewBottonConstraint.constant = - _inputView.bounds.size.height;
    [UIView animateWithDuration:.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.view layoutIfNeeded];
//        _inputTextView.text = [NSString stringWithFormat:@"@%@",model.rUserName];
    } completion:^(BOOL finished) {
        
    }];
}

/**
 弹出评论、回复输入
 */
- (void)popInputView:(NSNotification *)notice{
    _responseModel = notice.userInfo[@"model"];
    _momentModel = notice.userInfo[@"commentModel"];
    NSString *placeholder;
    if (_responseModel) {
        placeholder = [NSString stringWithFormat:@"@%@",_responseModel.rUserName];
    }else{
        placeholder = [NSString stringWithFormat:@"@%@",_momentModel.userName];
    }
    _isEdit = YES;
    _inputTextView.placeholderText = placeholder;
    [_inputTextView becomeFirstResponder];
}

#pragma mark -- YYTextView代理
- (BOOL)textView:(YYTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if( [@"\n" isEqualToString: text]){
        [textView resignFirstResponder];
        [self editDone];
        _isEdit = NO;
        return NO;
    }
    YYTextLayout *layout = textView.textLayout;
    CGFloat height = layout.textBoundingSize.height;
    if (height < 100) {
        _inputViewHeightConstraint.constant = 18+height;
        [UIView animateWithDuration:.1 animations:^{
            [self.inputView layoutIfNeeded];
        }];
    }
    return YES;
}

- (void)editDone{
    __weak typeof(self)weakSelf = self;
    NSString *text = _inputTextView.text;
    NSString *postID;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSLog(@"输入完成:%@",text);
    if (_responseModel) {
        ///回复评论
        [JCWebDataRequst responseTopicWithText:text
                                        postID:_responseModel.postID
                                      parentID:_responseModel.responseID
                                      complete:^(BOOL state, NSString *responseID) {
                                          if (state) {
                                              [hud hide:YES];
                                              [weakSelf addNewCommentResponse:responseID];
                                          }else{
                                              hud.labelText = @"请稍后再试";
                                              [hud hide:YES afterDelay:1];
                                          }
        }];
    }else{
        ///发表评论
        [JCWebDataRequst responseTopicWithText:text
                                        postID:_momentModel.topicID
                                      parentID:nil
                                      complete:^(BOOL state, NSString *responseID) {
                                          if (state) {
                                              [hud hide:YES];
                                              [weakSelf addNewCommentSuccess:responseID];
                                          }else{
                                              hud.labelText = @"请稍后再试";
                                              [hud hide:YES afterDelay:1];
                                          }
        }];
    }
}

/**
 新加一条评论
 @params            responseID      服务器返回的评论ID
 */
- (void)addNewCommentSuccess:(NSString *)responseID{
    JCMomentResponseModel *newComment = [JCMomentResponseModel creatNewCommentWithText:_inputTextView.text postID:_momentModel.topicID responseID:responseID];
    [_momentModel addCommentModel:newComment];
    _responseModel = nil;
    _momentModel = nil;
    _inputTextView.text = @"";
    _inputViewHeightConstraint.constant = 35;
    [_tableview reloadData];
}

/**
 新加一条回复
 @params            responseID      服务器返回的回复ID
 */
- (void)addNewCommentResponse:(NSString *)responseID{
    JCMomentResponseModel *newResponse = [JCMomentResponseModel creatNewResponseWithText:_inputTextView.text responseID:responseID commentModel:_responseModel];
    [_responseModel.momentModel addResponseModel:newResponse];
    _responseModel = nil;
    _momentModel = nil;
    _inputTextView.text = @"";
    _inputViewHeightConstraint.constant = 35;
    [_tableview reloadData];
}

/**
 加载数据
 */
- (void)loadData{
    __weak typeof(self)weakSelf = self;
    _pageIndex = 1;
    [JCWebDataRequst getTopicListWithType:0 page:_pageIndex complete:^(NSArray *topicModeList) {
        weakSelf.allMomentModel = [NSMutableArray arrayWithArray:topicModeList];
        [weakSelf.tableview reloadData];
    }];
}

/**
 下拉刷新数据
 */
- (void)dropRefreshData{
    __weak typeof(self)weakSelf = self;
    _pageIndex = 1;
    [JCWebDataRequst getTopicListWithType:0 page:_pageIndex complete:^(NSArray *topicModeList) {
        weakSelf.allMomentModel = [NSMutableArray arrayWithArray:topicModeList];
        [weakSelf.tableview reloadData];
        [weakSelf.tableview.mj_header endRefreshing];
    }];
}

/**
 上拉加载更多数据
 */
- (void)loadMore{
    __weak typeof(self)weakSelf = self;
    _pageIndex++;
    [JCWebDataRequst getTopicListWithType:0 page:_pageIndex complete:^(NSArray *topicModeList) {
        for (JCMomentsModel *model in topicModeList) {
            [weakSelf.allMomentModel addObject:model];
        }
        [weakSelf.tableview reloadData];
        [weakSelf.tableview.mj_header endRefreshing];
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_tableview.mj_footer endRefreshing];
    });
}

#pragma mark - tableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _allMomentModel.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JCMomentsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JCMomentsCell" forIndexPath:indexPath];
    JCMomentsModel *model = _allMomentModel[indexPath.row];
    [cell setModel:model indexPath:indexPath delegate:self];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    JCMomentsModel *model = _allMomentModel[indexPath.row];
    return model.showMoreSate == ShowMoreBtnSatePackUp?model.normalCellHeight:model.showMoreCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    return;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
}

#pragma mark - cell代理
/**
 查看更多文字
 */
- (void)watchMoreTextAction:(JCMomentsCell *)cell
                      model:(JCMomentsModel *)model
                  indexPath:(NSIndexPath *)indexPath{
    [self.tableview reloadData];
}

/**
 点赞动作
 */
- (void)editLikeAction:(JCMomentsCell *)cell
                 model:(JCMomentsModel *)model
             indexPath:(NSIndexPath *)indexPath{
    [self.tableview reloadData];
}

#pragma mark - scrollview代理
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (_isEdit == NO) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNoticeCancelAllEdit object:nil];
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNoticePopInputView object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
