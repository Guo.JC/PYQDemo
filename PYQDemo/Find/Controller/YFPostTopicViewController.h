//
//  YFPostTopicViewController.h
//  PhotoManager
//
//  Created by Coollang on 2017/9/5.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JCBaseViewController.h"

@class YFPhotoPickerManager;
@interface YFPostTopicViewController : JCBaseViewController

/** 图片选择器 */
@property (nonatomic, strong) YFPhotoPickerManager *photopickerManager;

@end
