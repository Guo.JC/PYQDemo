//
//  YFPhotoPickerManager.m
//  PhotoManager
//
//  Created by Coollang on 2017/9/5.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import "YFPhotoPickerManager.h"
#import "UIView+JCDrawTool.h"

@interface YFPhotoPickerManager ()<HXPhotoViewControllerDelegate,HXFullScreenCameraViewControllerDelegate,HXCameraViewControllerDelegate,HXVideoPreviewViewControllerDelegate>

@property (nonatomic, copy)DidCancelBlock cancelBlock;

@property (nonatomic, copy)DidNextBlock nextBlock;

@end

@implementation YFPhotoPickerManager

+ (instancetype)shareInstance {
    static YFPhotoPickerManager *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[YFPhotoPickerManager alloc] init];
    });
    return _instance;
}

- (HXPhotoManager *)photoManager {
    if (_photoManager == nil) {
        _photoManager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
        _photoManager.open3DTouchPreview = YES;
        _photoManager.cameraType = HXPhotoManagerCameraTypeFullScreen;
        _photoManager.openCamera = YES;
        _photoManager.outerCamera = YES;
        _photoManager.maxNum = 6;//最大选择数
        _photoManager.photoMaxNum = 6;//图片最大选择数
        _photoManager.videoMaxNum = 1; //视频最大选择数
        _photoManager.selectTogether = NO;
        _photoManager.saveSystemAblum = NO;
        _photoManager.isOriginal = NO;
        _photoManager.videoMaxDuration = 10;
        [self customizeHXUIManager:_photoManager.UIManager];
    }
    return _photoManager;
}

// 定制图像选择的UI
- (void)customizeHXUIManager:(HXPhotoUIManager *)uImanager {
    UIColor *barItemColor = UIColorFromHex(0xf0f0f0);
    uImanager.navBackgroundImageName = @"navBackgroundImage";
    uImanager.navLeftBtnTitleColor = barItemColor;
    uImanager.navTitleColor = barItemColor;
    uImanager.navRightBtnNormalTitleColor = barItemColor;
    uImanager.photosNumberTitleColor = barItemColor;
    uImanager.navRightBtnDisabledTitleColor = UIColorFromHex(0x999999);
    uImanager.navRightBtnDisabledBgColor = [UIColor clearColor];
    uImanager.photosNumberTitleColor = UIColorFromHex(0x666666);
    uImanager.navRightBtnBorderColor = [UIColor clearColor];
    uImanager.blurEffect = YES;
    uImanager.navRightBtnNormalBgColor = [UIColor clearColor];
}

- (void)showPickerControllerWithType:(kTapActionButtonType)tapType targetVc:(UIViewController *)targetVc  didNext:(DidNextBlock)nextBlock didCancel:(DidCancelBlock)cancelBlock {
    
    self.nextBlock = nextBlock;
    self.cancelBlock = cancelBlock;
    
    if (tapType == kTapAlbumButton) {
        self.photoManager.openCamera = NO;
        HXPhotoViewController *vc = [[HXPhotoViewController alloc] init];
        vc.manager = self.photoManager;
        vc.delegate = self;
        [targetVc presentViewController:[[UINavigationController alloc] initWithRootViewController:vc] animated:YES completion:nil];
    }else if(tapType == kTapCameraButton){
        self.photoManager.openCamera = YES;
        self.photoManager.videoMaxDuration = 10;
        if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [targetVc.view showImageHUDText:@"此设备不支持相机!"];
            return;
        }
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if (authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied) {
            
            UIAlertController *alertVc = [UIAlertController alertControllerWithTitle:@"无法使用相机" message:@"请在设置-隐私-相机中允许访问相机" preferredStyle:UIAlertControllerStyleAlert];
            [alertVc addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
            [alertVc addAction:[UIAlertAction actionWithTitle:@"设置" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }]];
            return;
        }
        
        HXCameraType type = 0;
        if (self.photoManager.type == HXPhotoManagerSelectedTypePhotoAndVideo) {
            if (self.photoManager.endSelectedVideos.count >= self.photoManager.videoMaxNum && self.photoManager.endSelectedPhotos.count < self.photoManager.photoMaxNum + self.photoManager.networkPhotoUrls.count) {
                type = HXCameraTypePhoto;
            }else if (self.photoManager.endSelectedPhotos.count >= self.photoManager.photoMaxNum + self.photoManager.networkPhotoUrls.count && self.photoManager.endSelectedVideos.count < self.photoManager.videoMaxNum) {
                type = HXCameraTypeVideo;
            }else if (self.photoManager.endSelectedPhotos.count + self.photoManager.endSelectedVideos.count >= self.photoManager.maxNum + self.photoManager.networkPhotoUrls.count) {
                [targetVc.view showImageHUDText:@"已达最大数!"];
                return;
            }else {
                type = HXCameraTypePhotoAndVideo;
            }
        }else if (self.photoManager.type == HXPhotoManagerSelectedTypePhoto) {
            if (self.photoManager.endSelectedPhotos.count >= self.photoManager.photoMaxNum + self.photoManager.networkPhotoUrls.count) {
                [targetVc.view showImageHUDText:@"照片已达最大数"];
                return;
            }
            type = HXCameraTypePhoto;
        }else if (self.photoManager.type == HXPhotoManagerSelectedTypeVideo) {
            if (self.photoManager.endSelectedVideos.count >= self.photoManager.videoMaxNum) {
                [targetVc.view showImageHUDText:@"视频已达最大数!"];
                return;
            }
            type = HXCameraTypeVideo;
        }
        
        if (self.photoManager.cameraType == HXPhotoManagerCameraTypeFullScreen) {
            HXFullScreenCameraViewController *vc1 = [[HXFullScreenCameraViewController alloc] init];
            vc1.delegate = self;
            vc1.type = type;
            vc1.photoManager = self.photoManager;
            if (self.photoManager.singleSelected) {
                
                [targetVc presentViewController:[[UINavigationController alloc] initWithRootViewController:vc1] animated:YES completion:nil];
            }else {
                [targetVc presentViewController:vc1 animated:YES completion:nil];
            }
        }else if (self.photoManager.cameraType == HXPhotoManagerCameraTypeHalfScreen) {
            HXCameraViewController *vc = [[HXCameraViewController alloc] init];
            vc.delegate = self;
            vc.type = type;
            vc.photoManager = self.photoManager;
            if (self.photoManager.singleSelected) {
                [targetVc presentViewController:[[UINavigationController alloc] initWithRootViewController:vc] animated:YES completion:nil];
            }else {
                [targetVc presentViewController:vc animated:YES completion:nil];
            }
        }
    }
}


// 照片和视频预览
- (void)previewVideoAndPhotoWithTargetVc:(UIViewController *)targetVc  model:(HXPhotoModel *)model {
    if ((model.type == HXPhotoModelMediaTypePhoto || model.type == HXPhotoModelMediaTypePhotoGif) || (model.type == HXPhotoModelMediaTypeCameraPhoto || model.type == HXPhotoModelMediaTypeLivePhoto)) {
        HXPhotoPreviewViewController *vc = [[HXPhotoPreviewViewController alloc] init];
        vc.selectedComplete = YES;
        vc.modelList = self.photoManager.endSelectedPhotos;
        vc.index = [self.photoManager.endSelectedPhotos indexOfObject:model];
        vc.manager = self.photoManager;
        vc.isPreview = YES;
        [targetVc presentViewController:vc animated:YES completion:nil];
    }else if (model.type == HXPhotoModelMediaTypeVideo){
        HXVideoPreviewViewController *vc = [[HXVideoPreviewViewController alloc] init];
        vc.manager = self.photoManager;
        vc.model = model;
        vc.selectedComplete = YES;
        [targetVc presentViewController:vc animated:YES completion:nil];
    }else if (model.type == HXPhotoModelMediaTypeCameraVideo) {
        HXVideoPreviewViewController *vc = [[HXVideoPreviewViewController alloc] init];
        vc.manager = self.photoManager;
        vc.model = model;
        vc.isCamera = YES;
        vc.selectedComplete = YES;
        [targetVc presentViewController:vc animated:YES completion:nil];
    }
}

- (void)previewVideoDidSelectedClick:(HXPhotoModel *)model {
    NSLog(@"previewVideoDidSelectedClick:%@",model);
}
- (void)previewVideoDidNextClick {
    NSLog(@"previewVideoDidNextClick");
}
// 代理方法

/**
 点击下一步执行的代理  数组里面装的都是 HXPhotoModel 对象
 @param allList 所有对象 - 之前选择的所有对象
 @param photos 图片对象 - 之前选择的所有图片
 @param videos 视频对象 - 之前选择的所有视频
 @param original 是否原图
 */
- (void)photoViewControllerDidNext:(NSArray<HXPhotoModel *> *)allList Photos:(NSArray<HXPhotoModel *> *)photos Videos:(NSArray<HXPhotoModel *> *)videos Original:(BOOL)original {
    if (self.nextBlock) {
        self.nextBlock(self.photoManager);
        self.nextBlock = nil;
    }
}

/**
 点击取消执行的代理
 */
- (void)photoViewControllerDidCancel {
    if (self.cancelBlock) {
        self.cancelBlock();
        self.cancelBlock = nil;
    }
}

- (void)fullScreenCameraDidNextClick:(HXPhotoModel *)model {
    [self cameraDidNextClick:model];
}

- (void)cameraDidNextClick:(HXPhotoModel *)model {
    // 判断类型
    if (model.type == HXPhotoModelMediaTypeCameraPhoto) {
        [self.photoManager.endCameraPhotos addObject:model];
        // 当选择图片个数没有达到最大个数时就添加到选中数组中
        if (self.photoManager.endSelectedPhotos.count != self.photoManager.photoMaxNum) {
            if (!self.photoManager.selectTogether) {
                if (self.photoManager.endSelectedList.count > 0) {
                    HXPhotoModel *phMd = self.photoManager.endSelectedList.firstObject;
                    if ((phMd.type == HXPhotoModelMediaTypePhoto || phMd.type == HXPhotoModelMediaTypeLivePhoto) || (phMd.type == HXPhotoModelMediaTypePhotoGif || phMd.type == HXPhotoModelMediaTypeCameraPhoto)) {
                        [self.photoManager.endSelectedCameraPhotos insertObject:model atIndex:0];
                        [self.photoManager.endSelectedPhotos addObject:model];
                        [self.photoManager.endSelectedList addObject:model];
                        [self.photoManager.endSelectedCameraList addObject:model];
                        model.selected = YES;
                    }
                }else {
                    [self.photoManager.endSelectedCameraPhotos insertObject:model atIndex:0];
                    [self.photoManager.endSelectedPhotos addObject:model];
                    [self.photoManager.endSelectedList addObject:model];
                    [self.photoManager.endSelectedCameraList addObject:model];
                    model.selected = YES;
                }
            }else {
                [self.photoManager.endSelectedCameraPhotos insertObject:model atIndex:0];
                [self.photoManager.endSelectedPhotos addObject:model];
                [self.photoManager.endSelectedList addObject:model];
                [self.photoManager.endSelectedCameraList addObject:model];
                model.selected = YES;
            }
        }
    }else if (model.type == HXPhotoModelMediaTypeCameraVideo) {
        [self.photoManager.endCameraVideos addObject:model];
        // 当选中视频个数没有达到最大个数时就添加到选中数组中
        if (self.photoManager.endSelectedVideos.count != self.photoManager.videoMaxNum) {
            if (!self.photoManager.selectTogether) {
                if (self.photoManager.endSelectedList.count > 0) {
                    HXPhotoModel *phMd = self.photoManager.endSelectedList.firstObject;
                    if (phMd.type == HXPhotoModelMediaTypeVideo || phMd.type == HXPhotoModelMediaTypeCameraVideo) {
                        [self.photoManager.endSelectedCameraVideos insertObject:model atIndex:0];
                        [self.photoManager.endSelectedVideos addObject:model];
                        [self.photoManager.endSelectedList addObject:model];
                        [self.photoManager.endSelectedCameraList addObject:model];
                        model.selected = YES;
                    }
                }else {
                    
                    [self.photoManager.endSelectedCameraVideos insertObject:model atIndex:0];
                    [self.photoManager.endSelectedVideos addObject:model];
                    [self.photoManager.endSelectedList addObject:model];
                    [self.photoManager.endSelectedCameraList addObject:model];
                    model.selected = YES;
                }
            }else {
                [self.photoManager.endSelectedCameraVideos insertObject:model atIndex:0];
                [self.photoManager.endSelectedVideos addObject:model];
                [self.photoManager.endSelectedList addObject:model];
                [self.photoManager.endSelectedCameraList addObject:model];
                model.selected = YES;
            }
        }
    }
    [self.photoManager.endCameraList addObject:model];
    [self photoViewControllerDidNext:self.photoManager.endSelectedList.mutableCopy Photos:self.photoManager.endSelectedPhotos.mutableCopy Videos:self.photoManager.endSelectedVideos.mutableCopy Original:self.photoManager.endIsOriginal];
}



@end
