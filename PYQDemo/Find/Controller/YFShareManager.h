//
//  YFShareManager.h
//  PYQDemo
//
//  Created by Coollang on 2017/9/8.
//  Copyright © 2017年 coollang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ShareSDK/ShareSDK.h>

@interface YFShareManager : NSObject

+ (instancetype)shareManager;

- (void)sharePlatformType:(SSDKPlatformType)type text:(NSString *)text images:(NSArray *)images;

@end
