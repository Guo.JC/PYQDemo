//
//  YFPostTopicViewController.m
//  PhotoManager
//
//  Created by Coollang on 2017/9/5.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import "YFPostTopicViewController.h"
#import "YFTopicSynShareCell.h"
#import "YFTopicTextviewCell.h"
#import "YFTopicPhotoCell.h"
#import "YFTopicModel.h"
#import "YFMoveCollection.h"
#import "Masonry.h"
#import "PhotoActionSheetViewController.h"
#import "HXPhotoViewController.h"
#import "HXFullScreenCameraViewController.h"
#import "YFPhotoPickerManager.h"
#import "HXPhotoModel.h"
#import "UIView+JCDrawTool.h"
#import <ShareSDK/ShareSDK.h>
#import "JCWebDataRequst.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <Qiniu/QiniuSDK.h>
#import "JCWebDataRequst+Find.h"
#import "JCLocation.h"
#import "JCGetLocation.h"
#import "JCMomentsModel.h"
#import "UIImage+HXExtension.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "YFShareManager.h"

#define kPhotoSpace 6
#define kPhotoLeftMargin 16
#define kPhotoCol 3        // 图片一行的列数

@interface YFPostTopicViewController ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,YFMoveCollectionDelegate>

@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic, strong) YFMoveCollection *collectionView;

@property (nonatomic, weak) UIView *bottomMaskView;
@property (nonatomic, strong) UIBarButtonItem *sendItem;
@property (nonatomic, strong,readonly) NSArray *dataSourceArr;

@property (nonatomic, strong) NSMutableArray *photoModelList;

@property (nonatomic, strong) YFTopicModel *textModel;
@property (nonatomic, strong) YFTopicModel *shareModel;
@property (nonatomic, strong) YFTopicModel *addModel;

/** 选择的是图片还是视频 */
@property (nonatomic, assign) HXPhotoModelMediaSubType selectedMediaType;
/** 需要同步到的平台 */
@property (nonatomic, assign)SSDKPlatformType  shareType;
/** 发送的文字内容 */
@property (nonatomic, strong) NSString *topicText;
/** 是否公开位置 */
@property (nonatomic, assign)BOOL openLocation;
/** 用户经纬度信息 */
@property (nonatomic, strong) CLLocation *userLocation;

/** 上传到QN后的图片链接 */
@property (nonatomic, strong) NSArray *uploadedImagesArr;

@end

@implementation YFPostTopicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.openLocation = YES; // 默认公开
    self.view.backgroundColor = [UIColor colorWithHex:0xf5f6fd];
    [self configuerNav];
    [self configuerMoveCollectionView];
    
    // 进来就去获取位置信息
    [self getUserLocation];
}
- (void)getUserLocation {
    if(self.userLocation == nil && self.openLocation){
        __weak typeof(self) weakSelf = self;
        [[JCLocation shareInstance] getCLLocationCallBack:^(CLLocation *location, NSError *error) {
            if (error) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf getUserLocation];
                });
            }else {
                weakSelf.userLocation = location;
            }
        }];
    }
}

- (void)configuerNav {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction:)];
    self.sendItem = [[UIBarButtonItem alloc] initWithTitle:@"发表" style:UIBarButtonItemStylePlain target:self action:@selector(postTopicAction:)];
    self.navigationItem.rightBarButtonItem = self.sendItem;
    [self UpdateSendItemState];
}

- (void)configuerMoveCollectionView {
    
    self.collectionView = [[YFMoveCollection alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
    [self.view addSubview:self.collectionView];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    UIView *maskView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight - 64, kScreenWidth, kScreenHeight)];
    self.bottomMaskView = maskView;
    [self.view addSubview:maskView];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.moveDelegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *backImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    backImg.image = [UIView drawBackgroundImageWith:backImg.bounds gradiendtRect:CGRectMake(0, backImg.bounds.size.height * 0.6, backImg.bounds.size.width, backImg.bounds.size.height * 0.4) startColor:[UIColor colorWithHex:0xf5f6fd] endColor:[UIColor colorWithHex:0xf5f6fd]];
    self.collectionView.backgroundView = backImg;
    
    /*注册Cell*/
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([YFTopicPhotoCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([YFTopicPhotoCell class])];
    [self.collectionView registerClass:[YFTopicTextviewCell class] forCellWithReuseIdentifier:NSStringFromClass([YFTopicTextviewCell class])];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([YFTopicSynShareCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([YFTopicSynShareCell class])];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"MoveCollectFooter"];
}

- (void)gotoSeletePhotoOrTakePhoto:(kTapActionButtonType)type {
    
    __weak typeof(self) weakSelf = self;
    
    [self.photopickerManager showPickerControllerWithType:type targetVc:self didNext:^(HXPhotoManager *manager) {
        [weakSelf updatePhotoModelList];
        [weakSelf UpdateSendItemState];
        [weakSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:1]];
    } didCancel:^{
        NSLog(@"取消选择");
    }];
}

- (void)updatePhotoModelList {
    
    if (_photoModelList.count > 0) {
        [_photoModelList removeAllObjects];
    }
    for (HXPhotoModel *photoModel in self.photopickerManager.photoManager.endSelectedList) {
        YFTopicModel *model = [[YFTopicModel alloc] initWithType:kYFTopicModelMediaTypePhoto];
        model.photoModel = photoModel;
        [self.photoModelList addObject:model];
    }
    if (self.photopickerManager.photoManager.endSelectedVideos.count > 0) {
        // 选择的是视频
        self.selectedMediaType = HXPhotoModelMediaSubTypeVideo;
    }else {
        // 选择的是照片
        self.selectedMediaType = HXPhotoModelMediaSubTypePhoto;
        if (self.photopickerManager.photoManager.endSelectedPhotos.count < 6) {
            [self.photoModelList addObject:self.addModel];
        }
    }
}

- (void)UpdateSendItemState {
    if (self.photopickerManager.photoManager.endSelectedList.count > 0 || self.topicText.length >= 1) {
        self.sendItem.enabled = YES;
    }else {
        self.sendItem.enabled = NO;
    }
}

#pragma mark - 发布话题相关接口调用逻辑
- (void)postTopicCompleted:(void(^)(BOOL success))completedBlock {
    
    [self uploadImagesQNComplated:^(NSArray *imageUrlArr, NSError *error) {
        
        if (error) {
            completedBlock(NO);
            return;
        }
        NSString *userLocationStr = (self.openLocation && self.userLocation) ? [NSString stringWithFormat:@"%f,%f",self.userLocation.coordinate.longitude,self.userLocation.coordinate.latitude]:@"";
        self.openLocation = NO;// 关闭获取定位
        [[JCLocation shareInstance] getCLLocationCallBack:^(CLLocation *location, NSError *error) {
            [JCWebDataRequst postTopicSubject:@"主题-iOS"
                                         text:self.topicText
                                   imageArray:imageUrlArr
                                     position:userLocationStr
                                     complete:^(WebRespondType respondType, id result) {
                                         if (respondType == WebRespondTypeSuccess) {
                                             if (completedBlock) {
                                                 completedBlock(YES);
                                             }
                                         }else{
                                             if (completedBlock) {
                                                 completedBlock(NO);
                                             }
                                         }
                                     }];
        }];
        
    }];
}

// 上传照片到七牛
- (void)uploadImagesQNComplated:(void(^)(NSArray *imageUrlArr,NSError *error))completedBlock {
    
    __weak typeof(self) weakSelf = self;
    
    if (self.photopickerManager.photoManager.endSelectedPhotos.count <= 0) {
        if (completedBlock) {
            completedBlock(@[],nil);
        }
        return;
    }
    // 获取照片
    [HXPhotoTools getImageForSelectedPhoto:weakSelf.photopickerManager.photoManager.endSelectedPhotos type:HXPhotoToolsFetchHDImageType completion:^(NSArray<UIImage *> *images) {
        
        [JCWebDataRequst getQiniuTokenComplete:^(NSString *token) {
            if (token == nil) {
                if (completedBlock) {
                    completedBlock(nil,[NSError errorWithDomain:@"获取七牛token失败" code:-1001 userInfo:nil]);
                }
                return;
            }
            // 华南
            QNUploadOption *option = [JCWebDataRequst qNOptionProgressHandler:^(NSString *key, float percent) {
                NSLog(@"------>%f",percent);
            }];
            // 2.获取Token 成功后上传图片到七牛
            [weakSelf upladToQNImages:images atIndex:0 token:token keys:[NSMutableArray array] filenamesArr:[NSMutableArray arrayWithCapacity:images.count] option:option completed:^(BOOL success, NSMutableArray *fileNameArr) {
                if (success) {
                    NSMutableArray *arrM = [NSMutableArray array];
                    [fileNameArr enumerateObjectsUsingBlock:^(NSString *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        [arrM addObject:[NSString stringWithFormat:@"http://staticvictor.coollang.com/%@",obj]];
                    }];
                    weakSelf.uploadedImagesArr = arrM.copy;
                    if (completedBlock) {
                         completedBlock(arrM.copy,nil);
                    }
                }else {
                    if (completedBlock) {
                        completedBlock(nil,[NSError errorWithDomain:@"上传图片失败" code:-1002 userInfo:nil]);
                    }
                }
            }];
        }];
        
    }];
}

/**
 批量上传图片至七牛服务器
 @param         images          图片数组
 @param         index           开始下标：传0即可
 @param         token           七牛token，服务器请求回来的
 @param         uploadManager   七牛manager
 @param         option          七牛option
 @param         keys            创建一个可变NSMutableArray空数组传进来
 */
- (void)upladToQNImages:(NSArray <UIImage *>*)images
                atIndex:(NSInteger)index
                  token:(NSString *)token
                   keys:(NSMutableArray *)keys
           filenamesArr:(NSMutableArray *)filenamesArr
                 option:(QNUploadOption *)option
              completed:(void(^)(BOOL success,NSMutableArray *fileNameArr))completedBlock {
    if (images.count <= 0) {
        if (completedBlock) {
            completedBlock(NO,nil);
        }
        return;
    }
    UIImage *image = images[index];
    __block NSInteger imageIndex = index;
    
    NSData *data = UIImageJPEGRepresentation(image, 0.1);
    
    NSTimeInterval time= [JCWebDataRequst getDeflautTimeStamp];
    
    //拼接图片上传后的路径： http://staticvictor.coollang.com/topicImage + 下划线 + uuid + 下划线 + 时间戳 + 下划线 + 序号 + 中划线 + 图片宽*图片高 + .jpg
    
    NSString *filename = [NSString stringWithFormat:@"topicImage_%@_%.f_%ld-%d*%d.%@",[JCUser currentUerID],time,imageIndex,(int)image.size.width,(int)image.size.height,@"jpg"];

    [filenamesArr addObject:filename];
    
    [[JCWebDataRequst qNUploadManager] putData:data key:filename token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        if (info.isOK) {
            [keys addObject:key];
            NSLog(@"idInex %ld,OK",index);
            imageIndex++;
            if (imageIndex >= images.count) {
                NSLog(@"上传完成,%@",filename);
                for (NSString *imgKey in keys) {
                    NSLog(@"%@",imgKey);
                }
                if (completedBlock) {
                    completedBlock(YES,filenamesArr);
                }
                return ;
            }
            [self upladToQNImages:images atIndex:imageIndex token:token keys:keys filenamesArr:filenamesArr option:option completed:completedBlock];
        }else{
            if (completedBlock) {
                completedBlock(NO,nil);
            }
            NSLog(@"上传失败,%@",filename);
        }
    } option:option];
}



#pragma mark - Action
- (void)backAction:(UIBarButtonItem *)sender {
     [self.view endEditing:YES];
    if (self.sendItem.enabled) {
        [self showAlertControllerTitle:nil message:@"退出此次编辑?" cancelTitle:@"取消" defalutTitle:@"确定" cancelAction:^(UIAlertAction *action) {
            
        } defalutAction:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)postTopicAction:(UIBarButtonItem *)sender {
    
//    self.topicText = @"澹然空水对斜晖，曲岛苍茫接翠微。\n波上马嘶看棹去，柳边人歇待船归。\n数丛沙草群鸥散，万顷江田一鹭飞。\n谁解乘舟寻范蠡，五湖烟水独忘机";
//    
//    self.uploadedImagesArr = @[
//                               [UIImage imageWithData:UIImageJPEGRepresentation([UIImage imageNamed:@"photo2l.jpg"], 0.1)],
//                               [UIImage imageWithData:UIImageJPEGRepresentation([UIImage imageNamed:@"photo3l.jpg"], 0.1)],
//                               [UIImage imageWithData:UIImageJPEGRepresentation([UIImage imageNamed:@"photo4l.jpg"], 0.1)],
//                               ];
    
//    [self dismissViewControllerAnimated:YES completion:^{
//        [[YFShareManager shareManager] sharePlatformType:self.shareType text:self.topicText images:self.uploadedImagesArr];
//    }];
  
    if (self.photopickerManager.photoManager.endSelectedVideos.count > 0) {
        [self showAlertControllerTitle:@"暂不支持发布视频" message:nil cancelTitle:@"确定" defalutTitle:nil cancelAction:^(UIAlertAction *action) {
            
        } defalutAction:nil];
        return;
    }
    
    if (self.topicText.length > 1000) {
        [self showAlertControllerTitle:@"发布的字数超过1000字" message:nil cancelTitle:@"确定" defalutTitle:nil cancelAction:^(UIAlertAction *action) {
            
        } defalutAction:nil];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self postTopicCompleted:^(BOOL success) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (success) {
            [self dismissViewControllerAnimated:YES completion:^{
//                [[YFShareManager shareManager] sharePlatformType:self.shareType text:self.topicText images:self.uploadedImagesArr];
            }];
        }else {
            [self showAlertControllerTitle:@"发布失败" message:nil cancelTitle:@"确定" defalutTitle:nil cancelAction:^(UIAlertAction *action) {
                
            } defalutAction:nil];
        }
    }];
}

#pragma mark - YFMoveCollectionDelegate
- (void)dragCellCollectionView:(YFMoveCollection *)collectionView newDataArrayAfterMove:(NSArray *)newDataArray {
    
   self.photoModelList = self.dataSourceArr[1];
}

- (NSArray *)dataSourceArrayOfCollectionView:(YFMoveCollection *)collectionView {
    return self.dataSourceArr;
}

#pragma mark -  UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.dataSourceArr.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dataSourceArr[section] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = nil;
    YFTopicModel *model = self.dataSourceArr[indexPath.section][indexPath.row];
    
    __weak typeof(self) weakSelf = self;
    
    if (indexPath.section == 0) {
        YFTopicTextviewCell *textCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([YFTopicTextviewCell class]) forIndexPath:indexPath];
        cell = textCell;
        textCell.model = model;
        textCell.textViewValueChangeBlock = ^(NSString *text) {
            self.topicText = text;
            NSLog(@"%@",self.topicText);
            [weakSelf UpdateSendItemState];
        };
    }else if (indexPath.section == 1){
        YFTopicPhotoCell *photoCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([YFTopicPhotoCell class]) forIndexPath:indexPath];
        cell = photoCell;
        [photoCell configerMoveViewCellWithItem:model];
    }else {
        YFTopicSynShareCell *shareCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([YFTopicSynShareCell class]) forIndexPath:indexPath];
        cell = shareCell;
        shareCell.model = model;
        shareCell.SynButtonClickBlock = ^(SSDKPlatformType type) {
            weakSelf.shareType = type;
        };
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    // 退出键盘
    [self.view endEditing:YES];
    
    YFTopicModel *model = self.dataSourceArr[indexPath.section][indexPath.row];
    __weak typeof(self) weakSelf = self;
    if (indexPath.section == 1 ) {
        if (model.type == kYFTopicModelMediaTypeAddPhoto) {
            // 添加照片
            [PhotoActionSheetViewController showInParentViewController:self animationType:DetailViewControllerPresentAnimationTypeUp actionBlock:^(kTapActionButtonType btnType) {
                [weakSelf gotoSeletePhotoOrTakePhoto:btnType];
            }].isTapCoverViewDismiss = YES;
        }else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            
            UIAlertAction *originalAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Preview", @"预览") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                // preview photos or video / 预览照片或者视频
                [weakSelf.photopickerManager previewVideoAndPhotoWithTargetVc:self model:model.photoModel];
            }];
            
            UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Delete", @"删除") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self deleteSeleteImageAtIndex:indexPath];
            }];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"取消") style:UIAlertActionStyleCancel handler:nil];
            
            [alertController addAction:originalAction];
            [alertController addAction:deleteAction];
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

- (void)deleteSeleteImageAtIndex:(NSIndexPath *)indexPath {
    
    YFTopicModel *model = self.dataSourceArr[indexPath.section][indexPath.row];
    [self.photopickerManager.photoManager deleteSpecifiedModel:model.photoModel];
    [self updatePhotoModelList];
    [self UpdateSendItemState];
    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:1]];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"MoveCollectFooter" forIndexPath:indexPath];
    footerView.backgroundColor = [UIColor colorWithHex:0xf5f6fd];
    return footerView;
}

/*设置Flowlayout*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return CGSizeMake(kScreenWidth, kAutoHei(120));
    }else if (indexPath.section == 1){
        CGFloat itemWH = (kScreenWidth - self.flowLayout.minimumLineSpacing * (kPhotoCol - 1) - self.flowLayout.sectionInset.left - self.flowLayout.sectionInset.right) / 3;
        itemWH = itemWH <= kAutoWid(78) ? itemWH:kAutoWid(78);
        return CGSizeMake(itemWH, itemWH);
    }else {
        return CGSizeMake(kScreenWidth, kAutoHei(94));
    }
    return CGSizeZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (section == 1) {
        return kPhotoSpace;
    }
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if (section == 1) {
        return kPhotoSpace;
    }
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (section == 1) {
        return UIEdgeInsetsMake(0, kPhotoLeftMargin, 14, kPhotoLeftMargin);
    }
    return UIEdgeInsetsZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    CGFloat footerH =  kHeight - (kAutoHei(120) + kAutoHei(78) + kAutoHei(94) + 64 + 14) + 6;
    if (section == 2) {
        if (self.photoModelList.count <= 4) {
            return CGSizeMake(kScreenWidth,footerH);
        }else {
            return CGSizeMake(kScreenWidth,(footerH - kAutoHei(78) - kPhotoSpace));
        }
    }
    return CGSizeZero;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - Setter & Getter
- (NSMutableArray *)dataSourceArr {
    return [NSMutableArray arrayWithObjects:@[self.textModel],[self photoModelList],@[self.shareModel], nil];
}

- (NSMutableArray *)photoModelList {
    if (_photoModelList == nil) {
        _photoModelList = [NSMutableArray array];
        [self updatePhotoModelList];
    }
    return _photoModelList;
}

- (UICollectionViewFlowLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout = [[UICollectionViewFlowLayout alloc] init];
    }
    return _flowLayout;
}

- (YFTopicModel *)addModel {
    if (_addModel == nil) {
        _addModel = [[YFTopicModel alloc] initWithType:kYFTopicModelMediaTypeAddPhoto];
    }
    return _addModel;
}

- (YFTopicModel *)textModel {
    if (_textModel == nil) {
        _textModel = [[YFTopicModel alloc] initWithType:kYFTopicModelMediaTypeText];
    }
    return _textModel;
}

- (YFTopicModel *)shareModel {
    if (_shareModel == nil) {
        _shareModel = [[YFTopicModel alloc] initWithType:kYFTopicModelMediaTypeSynShare];
    }
    return _shareModel;
}

- (YFPhotoPickerManager *)photopickerManager {
    if (_photopickerManager == nil) {
        _photopickerManager = [[YFPhotoPickerManager alloc] init];
    }
    return _photopickerManager;
}


@end
