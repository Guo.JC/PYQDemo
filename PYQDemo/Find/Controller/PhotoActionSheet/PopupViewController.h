//
//  PopupViewController.h
//  CoolMove
//
//  Created by CA on 14-8-23.
//  Copyright (c) 2014年 CA. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, DetailViewControllerAnimationType) {
    DetailViewControllerAnimationTypeSlide,
    DetailViewControllerAnimationTypeFade,
    DetailViewControllerAnimationTypeSlideRight
};

typedef NS_ENUM(NSUInteger, DetailViewControllerPresentAnimationType) {
    DetailViewControllerPresentAnimationTypePop,
    DetailViewControllerPresentAnimationTypeUp,
    DetailViewControllerPresentAnimationTypeDown,
    DetailViewControllerPresentAnimationTypeFade,
    DetailViewControllerPresentAnimationTypeFadeUp,
    DetailViewControllerPresentAnimationTypeLeft
};

@interface PopupViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *popupView;
- (void)presentInParentViewController:(UIViewController *)parentViewController;

- (void)presentInParentViewController:(UIViewController *)parentViewController animationType:(DetailViewControllerPresentAnimationType)animationType;

- (void)dismissFromParentViewControllerWithAnimationType:(DetailViewControllerAnimationType)animationType;

@end




