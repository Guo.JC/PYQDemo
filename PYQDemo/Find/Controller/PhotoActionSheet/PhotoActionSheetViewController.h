//
//  PhotoActionSheetViewController.h
//  PhotoManager
//
//  Created by Coollang on 2017/9/1.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import "PopupViewController.h"


typedef enum : NSUInteger {
    kTapCameraButton = 0,
    kTapAlbumButton = 1,
    kTapCacnelButton = -1,
} kTapActionButtonType;

typedef void(^TapButtonTypeBlock)(kTapActionButtonType btnType);

@interface PhotoActionSheetViewController : PopupViewController

/** 是否开启点击黑色蒙版退出 */
@property (nonatomic, assign)BOOL isTapCoverViewDismiss;

/**
 controller:需要显示在那个控制器上，nil: 不自动推出
 animationType:动画类型
 tapButtonBlock：操作回调
 */
+ (PhotoActionSheetViewController *)showInParentViewController:(UIViewController *)controller animationType:(DetailViewControllerPresentAnimationType)animationType actionBlock:(TapButtonTypeBlock)tapButtonBlock;




@end
