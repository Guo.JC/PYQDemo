//
//  PhotoActionSheetViewController.m
//  PhotoManager
//
//  Created by Coollang on 2017/9/1.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import "PhotoActionSheetViewController.h"

@interface PhotoActionSheetViewController ()

@property (weak, nonatomic) IBOutlet UIButton *topButton;

@property (weak, nonatomic) IBOutlet UIView *cameraAndAlbumSuperView;
@property (weak, nonatomic) IBOutlet UIView *cancelBtnSuperView;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIButton *albumButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

/** 用户点击按钮需要进行的操作 */
@property (nonatomic, copy)TapButtonTypeBlock tapButtonBlock;

@end

@implementation PhotoActionSheetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
    [self configureLocalization];
    self.popupView.layer.cornerRadius = 0;
}

+ (PhotoActionSheetViewController *)showInParentViewController:(UIViewController *)controller animationType:(DetailViewControllerPresentAnimationType)animationType actionBlock:(TapButtonTypeBlock)tapButtonBlock {
    PhotoActionSheetViewController *sheetVc = [[PhotoActionSheetViewController alloc] initWithNibName:@"PhotoActionSheetViewController" bundle:nil];
    
    // DetailViewControllerPresentAnimationTypeUp
    if (controller != nil) {
        [sheetVc presentInParentViewController:controller animationType:animationType];
    }
    sheetVc.tapButtonBlock = tapButtonBlock;
    return sheetVc;
}


- (void)setUpUI {
    UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    UIVisualEffectView *visualView = [[UIVisualEffectView alloc] initWithEffect:effect];
    CGFloat popViewH = (160.0/667) *[UIScreen mainScreen].bounds.size.height;
    visualView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, popViewH);
    [self.popupView insertSubview:visualView atIndex:0];
    
}

- (void)configureLocalization {
    [self.cameraButton setTitle:NSLocalizedString(@"Take Photo", nil) forState:UIControlStateNormal];
    [self.albumButton setTitle:NSLocalizedString(@"Choose From Album", nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
}

- (IBAction)topButtonClick:(UIButton *)sender {
    if (self.isTapCoverViewDismiss) {
        [self dismissVC];
        if (self.tapButtonBlock) {
            self.tapButtonBlock(kTapCacnelButton);
            self.tapButtonBlock = nil;
        }
    }
}

- (IBAction)takePhoto:(id)sender {
    if (self.tapButtonBlock) {
        self.tapButtonBlock(kTapCameraButton);
        self.tapButtonBlock = nil;
    }
    [self dismissVC];
}

- (IBAction)chooseFromAlbum:(id)sender {
    if (self.tapButtonBlock) {
        self.tapButtonBlock(kTapAlbumButton);
        self.tapButtonBlock = nil;
    }
    [self dismissVC];
}

- (IBAction)cancelButtonDidTouch:(id)sender {
    [self dismissVC];
    if (self.tapButtonBlock) {
        self.tapButtonBlock(kTapCacnelButton);
        self.tapButtonBlock = nil;
    }
}

- (void)dismissVC {
    [self dismissFromParentViewControllerWithAnimationType:DetailViewControllerAnimationTypeSlide];
}

@end
