//
//  JCMCLDataSource.m
//  PYQDemo
//
//  Created by Guo.JC on 2017/9/6.
//  Copyright © 2017年 coollang. All rights reserved.
//

#import "JCMCLDataSource.h"
#import "JCCommentCell.h"
#import "JCMomentsModel.h"
#import "JCMomentSetting.h"
#import "JCUserManager.h"

@interface JCMCLDataSource ()

@end

@implementation JCMCLDataSource

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _sourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JCCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JCCommentCell" forIndexPath:indexPath];
    [cell setMomentResponseModel:_sourceArray[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JCMomentResponseModel *model = _sourceArray[indexPath.row];
    BOOL isMyComment = [model.rUserID isEqualToString:[JCUser currentUerID]];
    BOOL isMyTopic = [model.momentModel.userID isEqualToString:[JCUser currentUerID]];
    BOOL isResponseMyComment = [model.quote.userID isEqualToString:[JCUser currentUerID]];
    if (isMyTopic) {///是我发布的帖子
        if (isMyComment) {
            ///不能回复自己的评论
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }else{
            ///只能回复自己帖子下，别人的评论
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNoticePopInputView object:nil userInfo:@{@"model":_sourceArray[indexPath.row]}];
        }
    }else{///不是我发布的帖子
        if (isResponseMyComment) {
            ///只能回复别人帖子下，主人回复自己的评论
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNoticePopInputView object:nil userInfo:@{@"model":_sourceArray[indexPath.row]}];
        }else{
            ///不能回复别人的评论及回复，以及自己回复主人的或者自己的评论
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ((JCMomentResponseModel *)_sourceArray[indexPath.row]).commentHeight;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
