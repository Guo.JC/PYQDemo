//
//  JCMCLDataSource.h
//  PYQDemo
//
//  Created by Guo.JC on 2017/9/6.
//  Copyright © 2017年 coollang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JCMomentResponseModel;

/**
 评论列表数据源
 */
@interface JCMCLDataSource : UIViewController<UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>

@property (nonatomic, strong) NSArray <JCMomentResponseModel *>*sourceArray;

@end
