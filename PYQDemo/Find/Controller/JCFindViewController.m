//
//  JCFindViewController.m
//  PYQDemo
//
//  Created by Guo.JC on 2017/9/2.
//  Copyright © 2017年 coollang. All rights reserved.
//

#import "JCFindViewController.h"
#import "PhotoActionSheetViewController.h"
#import "YFPhotoPickerManager.h"
#import "YFPostTopicViewController.h"
#import "JCMainNavigationController.h"

@interface JCFindViewController ()

@property (weak, nonatomic) IBOutlet UIView *rankingListView;
@property (weak, nonatomic) IBOutlet UIView *momentsView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rankingListViewLeftConstraint;


@property (nonatomic, strong) YFPhotoPickerManager *photopickerManager;

@end

@implementation JCFindViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(postTopic)];
}

- (void)postTopic {
    
    [PhotoActionSheetViewController showInParentViewController:[[UIApplication sharedApplication] keyWindow].rootViewController animationType:DetailViewControllerPresentAnimationTypeUp actionBlock:^(kTapActionButtonType btnType) {
        [self gotoSeletePhotoOrTakePhoto:btnType];
    }];
}

- (IBAction)switchAction:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0) {
        _rankingListViewLeftConstraint.constant = 0;
    }else{
        _rankingListViewLeftConstraint.constant = -kWidth;
    }
    [UIView animateKeyframesWithDuration:.5 delay:0 options:UIViewKeyframeAnimationOptionCalculationModePaced animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)gotoSeletePhotoOrTakePhoto:(kTapActionButtonType)type {
    
    if (self.photopickerManager != nil) {
        self.photopickerManager = nil;
    }
    YFPhotoPickerManager *photoManager = [[YFPhotoPickerManager alloc] init];
    self.photopickerManager = photoManager;
    
    [photoManager showPickerControllerWithType:type targetVc:self didNext:^(HXPhotoManager *manager) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.35 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            YFPostTopicViewController *topicVc = [[YFPostTopicViewController alloc] init];
            topicVc.photopickerManager = self.photopickerManager;
            JCMainNavigationController *navVc = [[JCMainNavigationController alloc] initWithRootViewController:topicVc];
            [self presentViewController:navVc animated:YES completion:nil];
        });
        
    } didCancel:^{
        NSLog(@"取消选择");
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
