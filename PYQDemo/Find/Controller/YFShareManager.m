//
//  YFShareManager.m
//  PYQDemo
//
//  Created by Coollang on 2017/9/8.
//  Copyright © 2017年 coollang. All rights reserved.
//

#import "YFShareManager.h"
#import "MBProgressHUD+HudTools.h"

@implementation YFShareManager

+ (instancetype)shareManager {
    static YFShareManager *_shareInstacnce;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareInstacnce = [YFShareManager new];
    });
    return _shareInstacnce;
}

- (void)sharePlatformType:(SSDKPlatformType)type text:(NSString *)text images:(NSArray *)images {
    
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    
    switch (type) {
        case SSDKPlatformSubTypeQZone:
//            [shareParams SSDKSetupShareParamsByText:text
//             
//                                             images:images //传入要分享的图片
//             
//                                                url:nil
//             
//                                              title:@"胜利App"
//             
//                                               type:SSDKContentTypeText|SSDKContentTypeImage];
            
             [shareParams SSDKSetupQQParamsByText:text title:@"shengliQQShared" url:nil audioFlashURL:nil videoFlashURL:nil thumbImage:nil images:images type:SSDKContentTypeText|SSDKContentTypeImage forPlatformSubType:SSDKPlatformSubTypeQZone];
            break;
        case SSDKPlatformSubTypeQQFriend:
            [shareParams SSDKSetupQQParamsByText:text title:@"shengliQQShared" url:nil audioFlashURL:nil videoFlashURL:nil thumbImage:nil images:images type:SSDKContentTypeText|SSDKContentTypeImage forPlatformSubType:SSDKPlatformSubTypeQQFriend];
            break;
        case SSDKPlatformSubTypeWechatSession:
//            [shareParams SSDKSetupShareParamsByText:text images:images url:[NSURL URLWithString:@"http://www.coollang.com"] title:@"胜利APP" type:SSDKContentTypeText|SSDKContentTypeImage];
           
            [shareParams SSDKSetupWeChatParamsByText:text title:@"胜利" url:nil thumbImage:nil image:images musicFileURL:nil extInfo:nil fileData:nil emoticonData:nil type:SSDKContentTypeImage|SSDKContentTypeText  forPlatformSubType:SSDKPlatformSubTypeWechatSession];
            break;
        case SSDKPlatformSubTypeWechatTimeline:
            [shareParams SSDKSetupWeChatParamsByText:text title:@"胜利" url:nil thumbImage:nil image:images musicFileURL:nil extInfo:nil fileData:nil emoticonData:nil type:SSDKContentTypeAuto  forPlatformSubType:SSDKPlatformSubTypeWechatTimeline];
            //[shareParams SSDKSetupWeChatParamsByText:text title:@"胜利App" url:nil thumbImage:images.firstObject image:images musicFileURL:nil extInfo:nil fileData:nil emoticonData:nil type:SSDKContentTypeAuto forPlatformSubType:SSDKPlatformSubTypeWechatTimeline];
            break;
            
           
        default:
             [shareParams SSDKSetupWeChatParamsByText:text title:@"胜利" url:nil thumbImage:nil image:images musicFileURL:nil extInfo:nil fileData:nil emoticonData:nil type:SSDKContentTypeAuto  forPlatformSubType:SSDKPlatformSubTypeWechatSession];
            break;
    }
    

    [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
        if (state == SSDKResponseStateSuccess) {
            [MBProgressHUD showSuccessHudInView:[UIApplication sharedApplication].keyWindow message:NSLocalizedString(@"Share Successfully", "") afterDelay:1.5];
        }
    }];
}



@end
