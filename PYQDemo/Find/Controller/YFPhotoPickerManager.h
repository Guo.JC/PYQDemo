//
//  YFPhotoPickerManager.h
//  PhotoManager
//
//  Created by Coollang on 2017/9/5.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HXPhotoViewController.h"
#import "HXFullScreenCameraViewController.h"
#import "PhotoActionSheetViewController.h"
#import "HXVideoPreviewViewController.h"
#import "HXPhotoPreviewViewController.h"

typedef void(^DidCancelBlock)();
typedef void(^DidNextBlock)(HXPhotoManager *manager);

@interface YFPhotoPickerManager : NSObject

@property (nonatomic, strong) HXPhotoManager *photoManager;

/**
 *  tapType:是选择照片还是去拍照
 *  targetVc:目标控制器
 *  nextBlock:点击下一步
 *  cancelBlock:点击取消
 */
- (void)showPickerControllerWithType:(kTapActionButtonType)tapType targetVc:(UIViewController *)targetVc  didNext:(DidNextBlock)nextBlock didCancel:(DidCancelBlock)cancelBlock;

// 照片和视频预览
- (void)previewVideoAndPhotoWithTargetVc:(UIViewController *)targetVc  model:(HXPhotoModel *)model;

@end
