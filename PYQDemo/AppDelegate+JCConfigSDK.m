//
//  AppDelegate+JCConfigSDK.m
//  Victor
//
//  Created by coollang on 17/3/1.
//  Copyright © 2017年 coollang. All rights reserved.
//

#import "AppDelegate+JCConfigSDK.h"
#import "AppDelegate.h"
#import "WebRequest.h"
#import "POP.h"
#import <AssetsLibrary/ALAssetsLibrary.h>
#import <Photos/Photos.h>
#import <CoreLocation/CoreLocation.h>
#import "JCDataHelper.h"
#import "JCStoryboardManager.h"
#import "JCMainNavigationController.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "JCFindViewController.h"
#import <ShareSDK/ShareSDK.h>
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKConnector/ShareSDKConnector.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import "WXApi.h"
#import "WeiboSDK.h"

#define         ShareSDKKEY         @"17669a2813d68"

#define         WechatKey           @"wxe3e65eb955a5ddf8"
#define         WechatScret         @"cc86e4141ed7da17e3ece59cc09d76c0"

#define         QQID                @"1105830427"
#define         QQKey               @"TEZFo4Exix8WcEDt"

#define         WeiboKey            @"2229249833"
#define         WeiboScret          @"4426cba86276d30f9d9822512256d1d3"



@implementation AppDelegate (JCConfigSDK)

- (void)configShareSDK{

    /**初始化ShareSDK应用
     @param activePlatforms
     使用的分享平台集合
     @param importHandler (onImport)
     导入回调处理，当某个平台的功能需要依赖原平台提供的SDK支持时，需要在此方法中对原平台SDK进行导入操作
     @param configurationHandler (onConfiguration)
     配置回调处理，在此方法中根据设置的platformType来填充应用配置信息
     */
    [WXApi registerApp:@"wxe3e65eb955a5ddf8"];
    [ShareSDK registerActivePlatforms:@[
                                        //                                        @(SSDKPlatformTypeSinaWeibo),
                                        @(SSDKPlatformTypeWechat),
                                        @(SSDKPlatformTypeQQ)
                                        ]
                             onImport:^(SSDKPlatformType platformType)
     {
         switch (platformType)
         {
             case SSDKPlatformTypeWechat:
                 [ShareSDKConnector connectWeChat:[WXApi class]];
                 break;
             case SSDKPlatformTypeQQ:
                 [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
                 break;
             case SSDKPlatformTypeSinaWeibo:
                 [ShareSDKConnector connectWeibo:[WeiboSDK class]];
                 break;
                 
             default:
                 break;
         }
     }
                      onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo)
     {
         switch (platformType)
         {
             case SSDKPlatformTypeSinaWeibo:
                 //设置新浪微博应用信息,其中authType设置为使用SSO＋Web形式授权
                 [appInfo SSDKSetupSinaWeiboByAppKey:WeiboKey
                                           appSecret:WeiboScret
                                         redirectUri:@"https://api.weibo.com/oauth2/default.html"
                                            authType:SSDKAuthTypeBoth];
                 break;
             case SSDKPlatformTypeWechat:
                 [appInfo SSDKSetupWeChatByAppId:WechatKey
                                       appSecret:WechatScret];
                 break;
             case SSDKPlatformTypeQQ:
                 [appInfo SSDKSetupQQByAppId:QQID
                                      appKey:QQKey
                                    authType:SSDKAuthTypeBoth];
                 break;
             default:
                 break;
         }
     }];
    
}

- (void)configSMSSDK{
    
}

- (void)migrationRealm{
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    // 设置新的架构版本。这个版本号必须高于之前所用的版本号（如果您之前从未设置过架构版本，那么这个版本号设置为 0）
    config.schemaVersion = kDB_VERSION;
    config.migrationBlock = ^(RLMMigration *migration, uint64_t oldSchemaVersion) {
        // 什么都不要做！Realm 会自行检测新增和需要移除的属性，然后自动更新硬盘上的数据库架构
    };
    [RLMRealmConfiguration setDefaultConfiguration:config];
    [RLMRealm defaultRealm];
}

- (void)setupWindow{
//    BOOL isNeedWelcome = [JCDataHelper isNeedWelcome];
    BOOL isHaveLogin = [JCDataHelper isLogin];
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    
    if (isHaveLogin) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isSaveUser"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //刷新登录
//        [JCWebDataRequst refreshLoginComplete:^(WebRespondType respondType, id result) {
//            if (respondType == WebRespondTypeSuccess) {
//                //下载徽章配置
//                [JCWebDataRequst getBadgeConfigComplete:^(WebRespondType respondType, id result) {
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"alreadyRefreshBadge" object:nil];
//                }];
//            }
//        }];
//        JCMainTabBarController *controller = [JCMainTabBarController new];
        UITabBarController *controller = [[[JCStoryboardManager sharedManager] storyboardWithType:StoryboardTypeFind] instantiateViewControllerWithIdentifier:@"tabbar"];
        self.window.rootViewController = controller;
    }
    else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isSaveUser"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        JCLoginViewController *loginVC = [[JCStoryboardManager storyboardForLogin] instantiateViewControllerWithIdentifier:SBID_LoginHomeVC];
        loginVC.fd_prefersNavigationBarHidden = YES;
        self.window.rootViewController = [[JCMainNavigationController alloc] initWithRootViewController:loginVC];
    }
    [self.window makeKeyAndVisible];
    /*
    if (isHaveLogin == NO) {
        self.startImageView = [[UIImageView alloc] initWithFrame:self.window.bounds];
        self.startImageView.image = [UIImage imageNamed:@"luanch"];
        WSProgressHUD *hud = [WSProgressHUD showOnView:self.startImageView andString:@""];
        [hud showWithMaskType:WSProgressHUDMaskTypeClear];
        [self.window addSubview:self.startImageView];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"-LoginHomeShowAnimation-" object:nil];
        });
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [hud dismiss];
            [hud removeFromSuperview];
            [self.startImageView removeFromSuperview];
            self.startImageView = nil;
        });
    }
    
    if (isHaveLogin && isNeedWelcome == NO) {
        self.startImageView = [[UIImageView alloc] initWithFrame:self.window.bounds];
        self.startImageView.image = [UIImage imageNamed:@"luanch"];
        WSProgressHUD *hud = [WSProgressHUD showOnView:self.startImageView andString:@""];
        [hud showWithMaskType:WSProgressHUDMaskTypeClear];
        
        [self.window addSubview:self.startImageView];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [hud dismiss];
            [hud removeFromSuperview];
            
            UIBezierPath *endMaskPath =  [UIBezierPath bezierPathWithArcCenter:CGPointMake(0, 0) radius:0.5 startAngle:0 endAngle:2*M_PI clockwise:YES];
            UIBezierPath *startMaskPath =  [UIBezierPath bezierPathWithArcCenter:CGPointMake(0, 0) radius:2*kHeight startAngle:0 endAngle:2*M_PI clockwise:YES];
            CAShapeLayer *maskLayer = [CAShapeLayer layer];
            maskLayer.path = endMaskPath.CGPath;
            self.startImageView.layer.mask = maskLayer;
            
            CABasicAnimation *maskLayerAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
            maskLayerAnimation.fromValue = (__bridge id)(startMaskPath.CGPath);
            maskLayerAnimation.toValue = (__bridge id)((endMaskPath.CGPath));
            maskLayerAnimation.duration = 0.8;
            maskLayerAnimation.timingFunction = [CAMediaTimingFunction  functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            [maskLayer addAnimation:maskLayerAnimation forKey:@"path"];
            maskLayerAnimation.delegate = self;
        });
    }
    
    if (isNeedWelcome) {
        __weak typeof(self)weakSelf = self;
        self.welcome = [[[JCStoryboardManager sharedManager] storyboardWithType:StoryboardTypeLogin] instantiateViewControllerWithIdentifier:SBID_LoginWelcomeVC];
        [self.window addSubview:self.welcome.view];
        self.welcome.dismiss = ^{
            weakSelf.welcome = nil;
        };
    }
     */
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    
    [self.startImageView removeFromSuperview];
    self.startImageView = nil;
}

- (void)setupUnity3DForApplication:(UIApplication *)application
     didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    
//    self.unityController = [[UnityAppController alloc] init];
//    [self.unityController application:application didFinishLaunchingWithOptions:launchOptions];
//    UnityGetMainWindow().hidden = YES;
}

- (void)touch_3D_config{
    
    // 创建标签的ICON图标。
    UIApplicationShortcutIcon *icon = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeCompose];
    // 创建一个标签，并配置相关属性。
    UIApplicationShortcutItem *item = [[UIApplicationShortcutItem alloc] initWithType:@"Test" localizedTitle:@"3D-Touch" localizedSubtitle:@"-Test-" icon:icon userInfo:nil];
    // 将标签添加进Application的shortcutItems中。
    [UIApplication sharedApplication].shortcutItems = @[item];
}

/**
 清理异常数据
 */
- (void)clearErrorDB{

}

- (void)importAddressDB{
}

/**
 提前获取 相机 、麦克风、相册权限
 */
- (void)getAuthorization{
    
    AVAuthorizationStatus videoAuthStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    AVAuthorizationStatus audioAuthStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    PHAuthorizationStatus photoAuthStatus = [PHPhotoLibrary authorizationStatus];
    
    if (videoAuthStatus == AVAuthorizationStatusNotDetermined){
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
//                JCLog(@"获得摄像头授权");
            }else{
//                JCLog(@"用户拒绝摄像头");
            }
        }];
    }
    
    if (audioAuthStatus == AVAuthorizationStatusNotDetermined) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
            if (granted) {
//                JCLog(@"获得麦克风授权");
            }else{
//                JCLog(@"获得拒绝麦克风");
            }
        }];
    }
    
    if (photoAuthStatus == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {///获得授权
//                JCLog(@"获得相册写入授权");
            }else{
//                JCLog(@"用户拒绝相册写入");
            }
        }];
    }
}
@end
