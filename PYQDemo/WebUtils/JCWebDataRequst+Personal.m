//
//  JCWebDataRequst+Personal.m
//  Victor
//
//  Created by Guo.JC on 2017/8/23.
//  Copyright © 2017年 coollang. All rights reserved.
//

#import "JCWebDataRequst+Personal.h"
#import "JCBaseWebUtils.h"
#import "WebRequest.h"
#import "JCUserManager.h"
//#import "JCBLEDevice.h"

@implementation JCWebDataRequst (Personal)

#pragma mark     ------------------------------------
#pragma mark ----------------个人中心---------------------
#pragma mark     ------------------------------------
///获取个人信息数据，同时更新用户信息数据库
+ (void)getUserInfo:(webRequestCallBack)complete{
    
    NSString *path = [self creatPathWithAPI:kGetUserInfo];
    [JCBaseWebUtils post:path andParams:nil andCallback:^(BOOL requestState, id obj) {
        if (requestState == YES) {
            NSInteger ret = [obj[@"ret"] integerValue];
            
            if (ret == WebRespondTypeSuccess) {
                NSDictionary *errDesc = obj[@"errDesc"];
                NSString *userID = errDesc[@"UserID"];
                
                JCUserInfo *userInfo = [JCUserInfo userInfoWithPrivateKey:userID];
                [userInfo userInfoWithUserInfoDictionary:errDesc];
                
                JCUser *user = [JCUser userWithPrivateKey:userID];
                [user updateUserInfo:userInfo];
                
                SAVE_CURRENT_USER_ID(user.userID);
                SAVE_LOGIN_STATE(YES);
                if (complete) {
                    complete(WebRespondTypeSuccess,errDesc[@"UserID"]);
                }
            }else if (ret == WebRespondTypeInvalidToken){
                [self tokenInvalid];
            }
            else if (ret == WebRespondTypeDataIsNil){
                complete(WebRespondTypeDataIsNil,nil);
            }
            else if (ret == WebRespondTypeNotLogin){
                SAVE_LOGIN_STATE(NO);
                complete(WebRespondTypeNotLogin, nil);
            }else{
                complete(WebRespondTypeFail, nil);
            }
        }
        else{
            complete(WebRespondTypeTimeOut, @"网络连接错误!");
            SAVE_LOGIN_STATE(NO);
        }
    }];
}
//
/////更新用户资料
//+(void)updateUserInfo:(NSDictionary *)params complete:(webRequestCallBack)complete{
//    
//    NSString *path = [self creatPathWithAPI:kSaveUserInfo];
//    [JCBaseWebUtils post:path andParams:params andCallback:^(BOOL requestState, id obj) {
//        if (requestState) {
//            NSInteger ret = [obj[@"ret"] integerValue];
//            if (ret == WebRespondTypeSuccess) {
//                complete(WebRespondTypeSuccess,nil);
//            }
//            else if (ret == WebRespondTypeNotLogin){
//                complete(WebRespondTypeNotLogin, nil);
//            }else if (ret == WebRespondTypeInvalidToken){
//                [self tokenInvalid];
//            }else{
//                complete(WebRespondTypeFail, nil);
//            }
//        }
//        else{
//            complete(WebRespondTypeTimeOut, nil);
//        }
//    }];
//}
/////获取上传七牛头像token
//+ (void)getLoadupHeaderToken:(webRequestCallBack)complete{
//    NSString *path = [self creatPathWithAPI:kGetHeaderToken];
//    [JCBaseWebUtils post:path andParams:nil andCallback:^(BOOL requestState, id obj) {
//        if (requestState == YES) {
//            NSInteger ret = [obj[@"ret"] integerValue];
//            if (ret == WebRespondTypeSuccess) {
//                
//                NSDictionary *errDesc = obj[@"errDesc"];
//                complete(WebRespondTypeSuccess,errDesc);
//            }
//            else if (ret == WebRespondTypeNotLogin){
//                complete(WebRespondTypeNotLogin, nil);
//            }
//            else if (ret == WebRespondTypeInvalidToken){
//                [self tokenInvalid];
//            }
//            else if (ret == WebRespondTypeVerifyCodeWrong){
//                complete(WebRespondTypeVerifyCodeWrong,nil);
//            }
//            else if (ret == WebRespondTypeMobServerError){
//                complete(WebRespondTypeMobServerError,nil);
//            }else{
//                complete(WebRespondTypeFail, nil);
//            }
//        }
//        else{
//            complete(WebRespondTypeTimeOut, nil);
//        }
//    }];
//}
//
///**
// *上传mac地址和固件版本、APP版本、生产信息
// */
//+ (void)uploadDeviceInfo:(JCBLEDevice *)device
//                complete:(webRequestCallBack)complete{
//    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
//    // app名称
////    NSString *app_Name = [infoDictionary objectForKey:@"CFBundleDisplayName"];
//    // app版本
//    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
//    // app build版本
////    NSString *app_build = [infoDictionary objectForKey:@"CFBundleVersion"];
//    NSString *path = [self creatPathWithAPI:kUploadMacVersion];
//    [JCBaseWebUtils post:path andParams:@{@"mac":device.macAddr,
//                                          @"hVer":device.version,
//                                          @"sVer":app_Version,
//                                          @"pVer":device.PCBVersion,
//                                          @"pBatch":@(device.productionBatch).stringValue,
//                                          @"fTime":@(device.burnTimestamp).stringValue}
//             andCallback:^(BOOL requestState, id obj) {
//                                              if (requestState == YES) {
//                                                  NSInteger ret = [obj[@"ret"] integerValue];
//                                                  if (ret == WebRespondTypeSuccess && complete) {
//                                                      NSDictionary *errDesc = obj[@"errDesc"];
//                                                      complete(WebRespondTypeSuccess,errDesc);
//                                                  }
//                                                  else if (ret == WebRespondTypeNotLogin && complete){
//                                                      complete(WebRespondTypeNotLogin, nil);
//                                                  }
//                                                  else if (ret == WebRespondTypeInvalidToken){
//                                                      [self tokenInvalid];
//                                                  }
//                                                  else if (ret == WebRespondTypeVerifyCodeWrong && complete){
//                                                      complete(WebRespondTypeVerifyCodeWrong,nil);
//                                                  }
//                                                  else if (ret == WebRespondTypeMobServerError && complete){
//                                                      complete(WebRespondTypeMobServerError,nil);
//                                                  }else{
//                                                      if (complete) {
//                                                          complete(WebRespondTypeFail, nil);
//                                                      }
//                                                  }
//                                              }
//                                              else{
//                                                  if (complete) {
//                                                      complete(WebRespondTypeTimeOut, nil);
//                                                  }
//                                              }
//                                          }];
//}


@end
