//
//  JCWebDataRequst+Personal.h
//  Victor
//
//  Created by Guo.JC on 2017/8/23.
//  Copyright © 2017年 coollang. All rights reserved.
//

#import "JCWebDataRequst.h"

@interface JCWebDataRequst (Personal)

#pragma mark     ------------------------------------
#pragma mark ----------------个人中心---------------------
#pragma mark     ------------------------------------
/**
 *获取用户个人信息数据
 */
+ (void)getUserInfo:(webRequestCallBack)complete;

///**
// *更新个人信息数据
// */
//+ (void)updateUserInfo:(NSDictionary *)params
//              complete:(webRequestCallBack)complete;
///**
// *获取上传头像凭证
// */
//+ (void)getLoadupHeaderToken:(webRequestCallBack)complete;
//
///**
// *上传mac地址和固件版本、APP版本、生产信息
// */
//+ (void)uploadDeviceInfo:(JCBLEDevice *)device
//                complete:(webRequestCallBack)complete;

@end
